## Auto
- auto x = 0, особенно хороша для сложных возвращаемых типов
- Проверка выведенных типов через static_assert(std::is_same<decltype(x), TSomeType>::value)
  - decltype(x) возвращает тип x
  - decltype(expr): T&, если expr соответствует l-value of type T, возвращает T в противном случае, когда r-value
  - decltype((x)) вернет decltype(x)&
  - Не вычисляет выражение в рантайме, а в compilation time выводит тип
- const int x = 0; auto y = x не перенесет const на y, нужно явно указать const auto y = x
- int m = 0; int& i = m; auto j = i выдаст int j, для переноса & надо явно написать auto& j = i
- auto& захватывает и const квалификатор
- auto&& работает как универсальная ссылка, сложная концепция, можно про нее не говорить
- for (const auto& x : a)
- for (auto it = a.begin(); it != a.end(); ++it)
- decltype на возвращаемое значение, когда оно неоднозначно выводится:
  - template <class T1, class T2> auto Function(T1 t1, T2 t2) { if (t1 < t2) { return t1 + 1; } else { return t2 + 1} } -> decltype(t1 + 1)
- std::vector<decltype(f())> results

## Lambda
- Мотивация, пример lambda, передача в std::sort
- Какая возможная реализация силами компилятора: уникальный класс со стейтом и вызовом TClass::Call
- Упрощенный синтаксис без (), если нет параметров
- Явное указание возвращаемого значения: где необходимо, когда можно обойтись без
- auto аргументы lambda и возможная реализация через шаблонный класс-функтор
- Передача аргумента по значению, по ссылке, по [arg = std::move(arg)]
- Изменение параметра - mutable lambda
- Как принимать lambda в своей функции - шаблонный параметр TFunction
- Передача lambda может быть тяжелой из-за стейта

## Structured bindings
- std::pair<int, double> p; auto [x, y] = p
- Аналогично со struct TStruct
- Аналогично с std::tuple
- for (const auto& [x, y] : vector of pairs)
- for (const auto& [key, value] : map)

## std::function
- std::function, чтобы принимать lambda без шаблонного параметра
- Чем плохо передавать шаблонный параметр?
  - Раздувается код, растет число сгенерированных классов, дольше компиляция
  - Тяжелее пользоваться классами с шаблонами: приходится везде указывать тип шаблонных аргументов: детали реализации выносятся наружу
  - std::function более тяжела, чем lambda, из-за виртуальной функции, динамической аллокации
  - std::function явно указывает сигнатуру
