#include <algorithm>
#include <iostream>
#include <string>

struct TCmp {
    bool operator()(int i1, int i2) {
        return i1 % 3 < i2 % 3;
    }
};

struct TPnt {
    void operator()(int i) {
        std::cout << i << " "; 
    }
};


int main()
{
    std::vector<int> i = {1,5,10};
    /*std::sort(i.begin(), i.end(), [](int i1, int i2) {
        return i1 % 3 < i2 % 3;
    });*/
    std::sort(i.begin(), i.end(), TCmp());
    std::for_each(i.begin(), i.end(), TPnt());
    std::cout << std::endl;
    return 0;
}
