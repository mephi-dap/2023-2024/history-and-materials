#include <iostream>

using namespace std;

/*
 * Упрощенный аналог программы tee. Читаем stdin, выводим в stdout
 */

int main() {
    string s;
    while (cin) {
        getline(cin, s);
        cout << s << '\n';
        cout.flush();
    }
    return 0;
}
