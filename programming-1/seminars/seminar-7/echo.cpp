#include <iostream>

using namespace std;

/*
 * Упрощенный аналог программы echo
 */

int main(int argc, const char** argv) {
    for (size_t i = 1; i < argc; ++i) {
        cout << argv[i];
        if (i < argc - 1) {
            cout << ' ';
        }
    }
    cout << endl;
    return 0;
}
