#include <string>
#include <iostream>

using namespace std;

void insert(string& s, char c, size_t counter, size_t& out) {
    s[out++] = c;
    if (counter < 2) {
        return;
    }
    const string counterString = std::to_string(counter);
    s.replace(out, counterString.size(), counterString);
    out += counterString.size();
}

void rle(string& s) {
    char c;
    char prevC = 0;
    size_t counter = 0;

    size_t out = 0;
    for (size_t in = 0; in < s.size(); ++in) {
        c = s[in];

        if (prevC == 0) {
            prevC = c;
            counter = 1;
            continue;
        }

        if (c == prevC) {
            ++counter;
            continue;
        }

        insert(s, prevC, counter, out);

        if (c == '\n') {
            prevC = 0;
            counter = 0;
            continue;
        }

        prevC = c;
        counter = 1;
    }

    if (counter > 0) {
        insert(s, prevC, counter, out);
    }

    s.resize(out);
}

int main() {
    string s = "kkkkwwwiiiiiiiiiiiifafafafvv";
    rle(s);
    cout << s << endl;
    return 0;
}
