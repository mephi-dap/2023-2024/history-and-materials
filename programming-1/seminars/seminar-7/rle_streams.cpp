#include <iostream>

// "dwddddwwrtttbb" -> "d1w1d4w2r1t3b2"

using namespace std;

int main() {
    char c;
    char prevC = 0;
    size_t counter = 0;
    while (cin) {
        cin.get(c);

        if (prevC == 0) {
            prevC = c;
            counter = 1;
            continue;
        }

        if (c == prevC) {
            ++counter;
            continue;
        }

        cout << prevC << counter;

        if (c == '\n') {
            prevC = 0;
            counter = 0;
            continue;
        }

        prevC = c;
        counter = 1;
    }

    if (counter > 0) {
        cout << prevC << counter;
    }

    return 0;
}
