#include <iostream>
#include <vector>
#include <optional>
#include <variant>

template<typename IterT, typename ValueT>
IterT Remove(IterT begin, IterT end, const ValueT& value) {
    IterT result = begin;
    for (IterT it = begin; it != end; ++it) {
        if (*it != value) {
            std::swap(*it, *result);
            ++result;
        }
    }
    return result;
}

template<typename IterT, typename PredT>
IterT RemoveIf(IterT begin, IterT end, PredT pred) {
    IterT result = begin;
    for (IterT it = begin; it != end; ++it) {
        if (!pred(*it)) {
            std::swap(*it, *result);
            ++result;
        }
    }
    return result;
}

struct IsEvenOrOdd {
    IsEvenOrOdd(bool even) : even_(even) {}

    bool operator ()(int value) {
        return value % 2 == (even_ ? 0 : 1);
    }

    bool even_;
};

bool IsEven(int value) {
    return value % 2 == 0;
}

bool IsOdd(int value) {
    return value % 2 == 1;
}

void DeleteSomeNumbersFromVector(std::vector<int>& a, bool even) {
//    a.erase(RemoveIf(
//            a.begin(),
//            a.end(),
//            [even](int value){return value % 2 == (even ? 0 : 1);}), a.end());
//    a.erase(RemoveIf(a.begin(), a.end(), IsEvenOrOdd(even)), a.end());
      a.erase(RemoveIf(a.begin(), a.end(), even ? IsEven : IsOdd), a.end());
}

template <typename IterT, typename PredT>
std::optional<typename IterT::value_type> FindValue(IterT begin, IterT end, PredT pred) {
    auto it = std::find_if(begin, end, pred);
    if (it != end) {
        return std::make_optional(*it);
    }
    return std::nullopt;
}

struct S1 {
    long long a;
    int b;
    int c[9];
    bool x, xx;
};

struct S2 {
    S1 s1;
    int y, y2, y3;
};

void ProcessNumbers(const std::vector<std::variant<int, std::string, float>>& maybe_numbers) {
    int int_sum = 0;
    float float_sum = 0;
    for (const auto& v : maybe_numbers) {
        if (v.index() == 0) {  // int
            int_sum += std::get<int>(v);
        } else if (v.index() == 1) {
            std::cout << std::get<std::string>(v);
        } else {
            float_sum += std::get<float>(v);
        }
    }
    std::cout << "int_sum: " << int_sum << ", float_sum: " << float_sum << std::endl;
}

void ProcessNumbers2(const std::vector<std::variant<int, std::string, float>>& maybe_numbers) {
    int int_sum = 0;
    float float_sum = 0;
    for (const auto& v : maybe_numbers) {
        if (auto value = std::get_if<int>(&v)) {
            int_sum += *value;
        } else if (auto value = std::get_if<std::string>(&v)) {
            std::cout << *value << ' ';
        } else if (auto value = std::get_if<float>(&v)) {
            float_sum += *value;
        } else {
            assert(false);
        }
    }
    std::cout << "int_sum: " << int_sum << ", float_sum: " << float_sum << std::endl;
}

int main() {
    std::vector<int> a = {1, 2, 3, 3, 4, 5, 5, 3, 4, 5, 10};
    DeleteSomeNumbersFromVector(a, true);
    for (auto el : a) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;

    std::optional<int> b = FindValue(a.begin(), a.end(), IsEven);
    std::cout << std::boolalpha;
    std::cout << b.has_value() << std::endl;
    std::cout << (bool)b << std::endl;
    std::cout << b.value_or(-1) << std::endl;
    std::cout << "\n\n\n\n";

    S1 s1 = {10, 30, {1,2,3}, false, true};
    S2 s2 = {s1, 10, 10, 10};

    std::cout
        << "s1 sizes:"
        << " " << sizeof(s1.a)
        << " " << sizeof(s1.b)
        << " " << sizeof(s1.c)
        << " " << sizeof(s1.x)
        << " " << sizeof(S1)
        << '\n';

    std::cout
            << "s2 sizes:"
            << " " << sizeof(s2.s1)
            << " " << sizeof(s2.y)
            << " " << sizeof(s2.y2)
            << " " << sizeof(s2)
            << '\n';

    std::cout
        << alignof(s1.a)
        << " " << alignof(s1.b)
        << " " << alignof(s1.c)
        << " " << alignof(s1.x)
        << " " << alignof(s1)
        << std::endl;


    std::cout
            << "s2 alignes:"
            << " " << alignof(s2.s1)
            << " " << alignof(s2.y)
            << " " << alignof(s2.y2)
            << " " << alignof(s2)
            << '\n';

    std::cout
        << &s1
        << " " << &s1.a << "(" << (size_t)&s1.a - (size_t)&s1 << ")"
        << " " << &s1.b << "(" << (size_t)&s1.b - (size_t)&s1 << ")"
        << " " << &s1.c << "(" << (size_t)&s1.c - (size_t)&s1 << ")"
        << " " << &s1.x << "(" << (size_t)&s1.x - (size_t)&s1 << ")"
        << " " << &s1.xx << "(" << (size_t)&s1.xx - (size_t)&s1 << ")"
        << '\n';

    std::variant<int, std::string, float> something = "adwd", bb = 12, cc = 4.5f;
    ProcessNumbers({something, bb, cc});
    ProcessNumbers2({something, bb, cc});

    something = 15;

    std::variant<double, float> f = 0.0f, d = 0.0;
    std::cout << "f index = " << f.index() << ", d index = " << d.index() << std::endl;
}
