#include <iostream>

class Vector {
   public:
    Vector(size_t size, int defaultValue)
            : size_(size)
            , capacity_(size_)
            , data_(new int[capacity_]) {
        for (size_t i = 0; i < size_; ++i) {
            data_[i] = defaultValue;
        }
    }

    Vector(const Vector& v)
            : size_(v.size_)
            , capacity_(v.capacity_)
            , data_(new int[capacity_]) {
        CopyData(v.data_, data_);
    }

    ~Vector() {
        delete[] data_;
    }

    size_t size() const {
        return size_;
    }

    size_t capacity() const {
        return capacity_;
    }

    int operator [](size_t index) const {
        return const_cast<Vector*>(this)->operator[](index);
    }

    int& operator [](size_t index) {
//        std::cerr << "getting element for index " << index << std::endl;
        return data_[index];
    }

    void push_back(int value);
    void pop_back();
    void resize(size_t size, int defaultValue);

    // оператор << можно переопределить не только для ostream
    Vector& operator <<(int value) {
        push_back(value);
        return *this;
    }

    // пре-декремент
    Vector& operator --() {
        pop_back();
        return *this;
    }

    // пост-декремент, отличается наличием аргумента
    Vector operator --(int) {
        Vector result(*this);
        pop_back();
        return result;
    }

   private:
    size_t size_, capacity_;
    int* data_;

    void CopyData(int* source, int* destination) {
        for (size_t i = 0; i < size_; ++i) {
            destination[i] = source[i];
        }
    }
};

void PrintVector(const Vector& v) {
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << " ";
    }
    std::cout << '\n';
}

std::ostream& operator <<(std::ostream& s, const Vector& v) {
    s << "Size: " << v.size() << "; " << "Capacity: " << v.capacity();
    for(size_t i = 0; i < 5 && i < v.size(); ++i) {
        s << " " << v[i];
    }
    return s;
}

void Foo() {
    Vector v(10, 0);
    v[5] = 15;
    v[7] = v[5] + 10;

    std::cout << v << std::endl;

    v << 42 << 150;
    PrintVector(v);

    std::cout << v << std::endl;
    std::cout << v-- << std::endl;
    std::cout << --v << std::endl;
}

void Vector::push_back(int value) {
    if (size_ == capacity_) {
        capacity_ = capacity_ > 0 ? capacity_ * 2 : 1;
        int* const newData = new int[capacity_];
        CopyData(data_, newData);
        delete[] data_;
        data_ = newData;
    }

    data_[size_] = value;
    ++size_;
}

void Vector::pop_back() {
    // like std::vector
    --size_;
}

void Vector::resize(size_t size, int defaultValue) {
    if (size_ >= size) {
        int* const newData = new int[size];
        capacity_ = size;
        size_ = size;
        CopyData(data_, newData);
        delete[] data_;
        data_ = newData;
        return;
    }

    int* const newData = new int[size];
    capacity_ = size;
    CopyData(data_, newData);
    delete[] data_;
    data_ = newData;

    for (size_t i = size_; i < size; ++i) {
        data_[i] = defaultValue;
    }

    size_ = size;
}

int main() {
    Foo();

    return 0;
}
