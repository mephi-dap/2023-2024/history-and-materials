#include <iostream>

//напишем шаблонную функцию, которая выводит медианный элемент

template <class T>
T mid(T& a,T& b,T& c) {
    if (a <= b && b <= c || c <= b && b <= a)
        return b;
    if (b <= a && a <= c || c <= a && a <= b)
        return a;
    return c;
}

// сделаем класс точки и научим ее работать с функцией mid
class Point {
    int x,y;
public:
    int Get() {
        return 0;
    }

    bool operator<=(const Point& p0) {
        return (y != p0.y) ? y <= p0.y : x <= p0.x; 
    }

    friend std::ostream& operator<<(std::ostream& st, const Point& p);
};

std::ostream& operator<<(std::ostream& st, const Point& p) {
    st << "(" << p.x << "," << p.y << ")";
    return st;
}

// переделаем вектор из прошлого семинара в шаблонный
template<class T>
class Vector {
   public:
    Vector(size_t size, T defaultValue)
            : size_(size)
            , capacity_(size_)
            , data_(new T[capacity_]) {
        for (size_t i = 0; i < size_; ++i) {
            data_[i] = defaultValue;
        }
    }

    Vector(const Vector& v)
            : size_(v.size_)
            , capacity_(v.capacity_)
            , data_(new T[capacity_]) {
        CopyData(v.data_, data_);
    }

    ~Vector() {
        delete[] data_;
    }

    size_t size() const {
        return size_;
    }

    size_t capacity() const {
        return capacity_;
    }

    int operator [](size_t index) const {
        return const_cast<Vector*>(this)->operator[](index);
    }

    int& operator [](size_t index) {
//        std::cerr << "getting element for index " << index << std::endl;
        return data_[index];
    }

    void push_back(int value);
    void pop_back();
    void resize(size_t size, int defaultValue);

    // оператор << можно переопределить не только для ostream
    Vector& operator <<(int value) {
        push_back(value);
        return *this;
    }

    // пре-декремент
    Vector& operator --() {
        pop_back();
        return *this;
    }

    // пост-декремент, отличается наличием аргумента
    Vector operator --(int) {
        Vector result(*this);
        pop_back();
        return result;
    }

   private:
    size_t size_, capacity_;
    T* data_;

    void CopyData(int* source, int* destination) {
        for (size_t i = 0; i < size_; ++i) {
            destination[i] = source[i];
        }
    }
};

template<class M>
void PrintVector(const Vector<M>& v) {
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << " ";
    }
    std::cout << '\n';
}


int main() {
    
    Point p1,p2,p3;
    std::cout << mid(p1,p2,p3);
    return 0;
}
