// Напишите класс-функтор, который соответствует лямбде
// int a, b, c;
// [&a, b, c = std::move(c)] (int d)
#include <algorithm>
#include <functional>
#include <string>
#include <iostream>
#include <chrono>

struct Func {
    Func(int& a, int b, int&& c) : a(a), b(b), c(std::move(c)) {}

    void operator () (int d) {
        a += d + b + c;
    }

    int& a;
    int b;
    int c;
};

struct User {
    size_t id;
    std::string nickname;
    size_t coins;
    bool deleted = false;
};

class Signal {
public:
    using CallbackT = std::function<void(User*)>;

    void AddCallback(const CallbackT& func) {
        functions_.push_back(func);
    }

    void Trigger(User* user) {
        for (const auto& f : functions_) {
            f(user);
        }
    }
private:
    std::vector<CallbackT> functions_;
};

class UsersDB {
   public:
    void CreateUser(const std::string& nickname) {
        User user{.id=users_.size(), .nickname=nickname, .coins=0};
        userPreCreated_.Trigger(&user);
        users_.push_back(std::move(user));
        userPostCreated_.Trigger(&users_.back());
    }

    void DeleteUser(size_t id) {
        users_[id].deleted = true;
        userDeleted_.Trigger(&users_[id]);
    }

    void AddCreateUserPreCallback(const Signal::CallbackT& f) {
        userPreCreated_.AddCallback(f);
    }

    void AddCreateUserPostCallback(const Signal::CallbackT& f) {
        userPostCreated_.AddCallback(f);
    }

    void AddDeleteUserCallback(const Signal::CallbackT& f) {
        userDeleted_.AddCallback(f);
    }

    // ...
   private:
    std::vector<User> users_;
    Signal userPreCreated_, userPostCreated_, userDeleted_;
};

void ConnectSignals(UsersDB& usersDb) {
    usersDb.AddCreateUserPostCallback([](User* user) {
        std::cout << "user #" << user->id
                  << " with nickname " << user->nickname
                  << " created\n";
    });

    usersDb.AddCreateUserPreCallback([](User* user) {
        user->nickname += "_pro";
    });

    usersDb.AddDeleteUserCallback([](User* user) {
        std::cout << "user #" << user->id << " deleted\n";
    });
}

class LoggedWrapper {
   public:
    using FuncT = std::function<int(int, int)>;
    LoggedWrapper(const FuncT& f) : f_(f) {}

    int operator() (int a, int b) const {
        std::cout << "called function with args " << a << ", " << b << std::endl;
        int result = f_(a, b);
        std::cout << "result = " << result << std::endl;
        return result;
    }

   private:
    FuncT f_;
};

auto WrapIntoTimer(std::function<int(int,int)> f) {
    return [f = std::move(f)](int a, int b) {
        auto before = std::chrono::high_resolution_clock::now();
        int result = f(a, b);

        std::chrono::milliseconds diff =
            std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - before);

        std::cout << diff.count() << "ms\n";
        return result;
    };
}

int mult(int a, int b) {
    return a * b;
}

auto Bind(std::function<int(int, int)> f, int value) {
    return [f = std::move(f), value] (int a) {
        return f(value, a);
    };
}

auto Comp(std::function<int(int)> f, std::function<int(int)>g) {
    return [f = std::move(f), g = std::move(g)] (int arg) {
        return f(g(arg));
    };
}

class Foo {
   public:
    Foo(int a) : a_(a) {}
    virtual ~Foo() = default;

    virtual void foo() {
        std::cout << "Foo::foo says: " << a_ << "\n";
    };

    void bar() {
        std::cout << "Foo::bar\n";
    }

   protected:
    int a_;
};

// ... 1234 vfptr 0x1 0x2 ...

class Bar : public Foo {
   public:
    Bar(int a, int b) : Foo(a), b_(b){}

    void foo() override {
        std::cout << "Bar::foo says: " << a_ + b_ << "\n";
        Foo::foo();
    }

    void bar() {
        std::cout << "Bar::bar\n";
    }

   private:
    int b_;
};

// ... 1234 987 vfptr 0x3 0x4 ...
// Foo* f = new Foo(...);
// Foo* f = new Bar(...);
// f->foo();
// delete f;

std::shared_ptr<Foo> make_bar(int a, int b) {
    return std::make_shared<Bar>(a, b);
}

int main() {
    UsersDB usersDb;
    ConnectSignals(usersDb);

    usersDb.CreateUser("P>|<ABb|u");
    usersDb.CreateUser("IIeJIMeHb");

    usersDb.DeleteUser(0);

    std::cout << (mult(5, 3) == LoggedWrapper(mult)(5, 3)) << std::endl;

    int c = WrapIntoTimer([](int a, int b){
        int sum = 0;
        for (int i = 0; i < a; i += b) {
            sum += b;
        }
        return sum;
    })(1000000000, 1);
    std::cout << c << std::endl;

    std::cout << "mult(5, 3) = " << mult(5 ,3) << std::endl;

    auto mult_5 = Bind(mult, 5);
    std::cout << "mult_5(3) = mult(5, 3) = " << mult_5(3) << std::endl;

//    auto plus_5 = std::bind(
//        [](int a, int b) {return a + b;},
//        5,
//        std::placeholders::_1);

    auto plus = [](int a, int b) {return a + b;};
    auto plus_5 = [plus](int a){return plus(5, a);};

    std::cout << "plus_5(3) = " << plus_5(3) << std::endl;

    auto mult_5_and_plus_5 = Comp(mult_5, plus_5);
    std::cout << "mult_5_and_plus_5(3) = " << mult_5_and_plus_5(3) << std::endl;

    auto divide = [](float a, float b){return a / b;};
    auto divide_10 = std::bind(divide, std::placeholders::_1, 10);
    std::cout << "divide_10(5) = " << divide_10(5) << std::endl;

    auto cmp = [](const auto& lhs, const auto& rhs) -> bool {
        return lhs < rhs;
    };

    auto print_container = [](auto begin, auto end) {
        for (auto it = begin; it != end; ++it) {
            std::cout << *it << ' ';
        }
        std::cout << std::endl;
    };

    std::vector<int> values = {1, 5, 2, 15, 8, 10};
    std::sort(values.begin(), values.end(), cmp);
    auto print_from_begin = std::bind(print_container, values.begin(), std::placeholders::_1);
    print_from_begin(std::find(values.begin(), values.end(), 10));

    std::vector<float> values_float = {1.5, 5.2, 2.1, 15.9, 8, 10};
    std::sort(values_float.begin(), values_float.end(), cmp);
    print_container(values_float.begin(), values_float.end());

    std::shared_ptr<Foo> bar = make_bar(1, 2);
    bar->foo();
    bar->bar();

    return 0;
}
