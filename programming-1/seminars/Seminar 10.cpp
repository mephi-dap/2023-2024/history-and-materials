#include <iostream>
#include <vector>

template <typename T>
struct SharedData {
    T* ptr_;
    size_t shared_counter_;
    size_t weak_counter_;
};

template <typename T>
class WeakPtr;

template<typename T>
class SharedPtr {
   public:
    SharedPtr() : data_(new SharedData<T>{.ptr_=new T(), .shared_counter_=1, .weak_counter_=0}) {
//        std::cout << "Initializing shared counter" << std::endl;
    }

    SharedPtr(const SharedPtr& other) : data_(other.data_) {
        ++data_->shared_counter_;
//        std::cout << "Increasing shared counter" << std::endl;
    }

    SharedPtr(T* ptr) : data_(new SharedData<T>{.ptr_=ptr, .shared_counter_=1, .weak_counter_=0}) {
//        std::cout << "Initializing shared counter" << std::endl;
    }

    ~SharedPtr() {
        --data_->shared_counter_;
//        std::cout << "Decreasing shared counter" << std::endl;
        if (data_->shared_counter_ == 0 && data_->ptr_ != nullptr) {
            delete data_->ptr_;
            data_->ptr_ = nullptr;
//            std::cout << "Delete ptr_" << std::endl;
        }
        if (data_->shared_counter_ + data_->weak_counter_ == 0) {
            delete data_;
//            std::cout << "Delete data_" << std::endl;
        }
    }

    T& operator *() {
        return *data_->ptr_;
    }

    const T& operator *() const {
        return *data_->ptr_;
    }

    T* operator ->() {
        return data_->ptr_;
    }

    const T* operator ->() const {
        return data_->ptr_;
    }

    T* Get() {
        return data_->ptr_;
    }

    const T* Get() const {
        return data_->ptr_;
    }

    operator bool() const {
        return data_->ptr_ != nullptr;
    }

private:
    SharedData<T>* data_;

    friend WeakPtr<T>;

    SharedPtr(SharedData<T>* data) : data_(data) {
//        std::cout << "Increasing shared counter" << std::endl;
        ++data_->shared_counter_;
    }
};

template <typename T>
class WeakPtr {
   public:
    WeakPtr(const SharedPtr<T> ptr) : data_(ptr.data_) {
        ++data_->weak_counter_;
//        std::cout << "Increasing weak counter" << std::endl;
    }

    WeakPtr(const WeakPtr& other) : data_(other.data_) {
        ++data_->weak_counter_;
//        std::cout << "Increasing weak counter" << std::endl;
    }

    ~WeakPtr() {
        --data_->weak_counter_;
//        std::cout << "Decreasing weak counter" << std::endl;
        if (data_->shared_counter_ + data_->weak_counter_ == 0) {
            delete data_;
//            std::cout << "Delete data_" << std::endl;
        }
    }

    SharedPtr<T> Lock() {
        return SharedPtr<T>(data_);
    }

    const SharedPtr<T> Lock() const {
        return SharedPtr<T>(data_);
    }
   private:
    SharedData<T>* data_;
};

SharedPtr<int> Foo(int x) {
    SharedPtr<int> result;
    *result = x;
    return result;
}

class CallsCounter {
   public:
    CallsCounter(const std::string& name) : name_(name), counter_(0) {}

    void Inc() {
        ++counter_;
    }

    void PrintCounter() const {
        std::cout << "counter " << name_ << " = " << counter_ << std::endl;
    }
   private:
    std::string name_;
    size_t counter_;
};

class StatsCollector {
   public:
    void AddCounter(SharedPtr<CallsCounter> counter) {
        counters_.push_back(counter);
    }

    void PrintStats() {
        std::vector<WeakPtr<CallsCounter>> live_counters;
        for (const auto& weak_counter : counters_) {
            if (const SharedPtr<CallsCounter> counter = weak_counter.Lock()) {
                counter->PrintCounter();  // (*counter).PrintCounter();
                live_counters.push_back(counter);
            }
        }
        std::cout << std::endl;
        std::swap(counters_, live_counters);
    }
   private:
    std::vector<WeakPtr<CallsCounter>> counters_;
};

int main() {
    SharedPtr<CallsCounter>
        request1(new CallsCounter("request1")),
        request2(new CallsCounter("request2")),
        request3(new CallsCounter("request3"));

    StatsCollector collector;
    collector.AddCounter(request1);
    collector.AddCounter(request2);
    collector.AddCounter(request3);

    collector.PrintStats();

    request1->Inc();
    request3->Inc();
    request3->Inc();
    collector.PrintStats();

    {
        SharedPtr<CallsCounter> request4 = new CallsCounter("request4");
        collector.AddCounter(request4);
        collector.PrintStats();

        request4->Inc();
        request4->Inc();
        request4->Inc();
        collector.PrintStats();
    }

    collector.PrintStats();

    return 0;
}
