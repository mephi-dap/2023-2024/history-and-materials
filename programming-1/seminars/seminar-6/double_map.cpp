#include <map>
#include <string>
#include <utility>
#include <iostream>
using namespace std;


template<class K, class V>
class DoubleMap {
private:
    using InnerMap = map<K, V>;
    using OutterMap = map<K, InnerMap>;

    OutterMap storage;

public:
    class iterator {
    public:
        using InnerIterator = typename InnerMap::iterator;
        using OutterIterator = typename OutterMap::iterator;
    
    private:
        InnerIterator in_it;
        OutterIterator out_it;
        OutterIterator out_end;

    public:
        iterator(InnerIterator _in_it, OutterIterator _out_it, OutterIterator _out_end)
            : in_it(_in_it), out_it(_out_it), out_end(_out_end) {
            if (out_it == out_end) {
                in_it = InnerIterator();
            }
        }

        pair<K, pair<K, V>> operator*() const {
            return {
                out_it->first,
                {in_it->first, in_it->second}
            };
        }

        iterator operator++() {
            ++in_it;
            if (in_it != out_it->second.end()) {
                return *this;
            }

            ++out_it;
            while (out_it != out_end && out_it->second.empty()) {
                ++out_it;
            }

            if (out_it != out_end) {
                in_it = out_it->second.begin();
                return *this;
            }

            in_it = InnerIterator();
            return *this;
        }

        bool operator!=(const iterator& other) const {
            return out_it != other.out_it || in_it != other.in_it;
        }
    };

    InnerMap& operator[](const K& key) {
        return storage[key];
    }

    iterator begin() {
        return iterator(
            storage.begin()->second.begin(),
            storage.begin(),
            storage.end()
        );
    }

    iterator end() {
        typename iterator::InnerIterator it;
        return iterator(
            it,
            storage.end(),
            storage.end()
        );
    }
    // .end():
    //      {k1: {k2: v2, k3: v3},
    //       k4: {k5: v5, k6: v6, k7: v7}
    //               <-- out_it
    //      }
    //      in_it = invalid
};

int main() {
    DoubleMap<int, string> dmap;

    dmap[0][0] = "kek";
    dmap[4][10] = "lol";

    for (const auto& v : dmap) {
        cout << v.first << ' ' << v.second.first << ' ' << v.second.second << endl;
    }
}