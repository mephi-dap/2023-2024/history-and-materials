#include <string>
#include <vector>
#include <iostream>
#include <unordered_set>
#include <queue>

class User;

class FeedEvent {
   public:
    FeedEvent(const std::string& message) : message_(message) {}

    const std::string& GetMessage() {
        return message_;
    }
   private:
    std::string message_;
    friend std::ostream& operator <<(std::ostream& os, const FeedEvent& event);
};

std::ostream& operator <<(std::ostream& os, const FeedEvent& event) {
    return os << event.message_;
}

class Feed {
   public:
    Feed(User* owner) : owner_(owner) {}

    void AddEvent(const std::string& message) {
        events_.emplace_back(message);
    }

    User* GetOwner() const {
        return owner_;
    }

   private:
    User* owner_;
    std::vector<FeedEvent> events_;

    friend std::ostream& operator <<(std::ostream& os, const Feed& feed);
};

std::ostream& operator <<(std::ostream& os, const Feed& feed) {
    for (const auto& event : feed.events_) {
        os << event << '\n';
    }
    return os;
}

class User {
   public:
    User(const std::string& nickname) : nickname_(nickname), feed_(this) {}

    void Follow(User* follower) {
        if (followers_.insert(follower).second) {
            follower->followees_.insert(this);

            feed_.AddEvent("User " + follower->GetNickname() + " is following me");
            follower->feed_.AddEvent("I am following " + GetNickname());
        }
    }

    void Unfollow(User* follower) {
        if (followers_.erase(follower)) {
            follower->followees_.erase(this);

            feed_.AddEvent("User " + follower->GetNickname() + " is not following me");
            follower->feed_.AddEvent("I am not following " + GetNickname());
        }
    }

    size_t FollowersCount() const {
        return followers_.size();
    }

    size_t FolloweesCount() const {
        return followees_.size();
    }

    const std::string GetNickname() const {
        return nickname_;
    }

    void PrintFeed() const {
        std::cout << "Feed for " << nickname_ << ":\n" << feed_;
    }

    const auto& GetFollowers() const {
        return followers_;
    }

    const auto& GetFollowees() const {
        return followees_;
    }
   private:
    std::string nickname_;
    std::unordered_set<User*> followers_, followees_;
    Feed feed_;
};

template <typename GraphTraits>
void bfs(
        typename GraphTraits::NodeType* node,
        std::function<bool(const typename GraphTraits::NodeType*, size_t)> visitedNodeProcessor) {
    using NodeT = typename GraphTraits::NodeType;
    std::queue<const NodeT*> q;
    std::unordered_map<const NodeT*, size_t> visited;

    q.push(node);
    visited.emplace(node, 0);

    while (!q.empty()) {
        const NodeT* const currentNode = q.front();
        q.pop();

        if (visitedNodeProcessor(currentNode, visited[currentNode])) {
            for (const NodeT* neighbour : GraphTraits::GetNeighbours(currentNode)) {
                if (visited.emplace(neighbour, visited[currentNode] + 1).second) {
                    q.push(neighbour);
                }
            }
        }
    }
}

class FollowersGraphTraits {
   public:
    using NodeType = User;

    static auto GetNeighbours(const NodeType* followee) {
        return followee->GetFollowers();
    }
};

bool IsATransitiveFollower(User* followee, User* follower, size_t maxHandshakesCount) {
    bool result = false;
    auto processUser = [follower, maxHandshakesCount, &result] (const User* currentUser, size_t handshakesCount) {
        if (currentUser == follower) {
            result = true;
            return false;
        }
        return handshakesCount < maxHandshakesCount;
    };

    bfs<FollowersGraphTraits>(followee, processUser);

    return result;
}

int main() {
    User user1("IIeJIMeHb"), user2("P>|<aBbIu");
    user1.Follow(&user2);
    user1.Follow(&user2);

    User user3("naPeHb_u3_qp0pcaWa");
    user2.Follow(&user3);

    std::cout << user1.GetNickname() << " has " << user1.FollowersCount() << " followers\n";
    std::cout << user2.GetNickname() << " has " << user2.FolloweesCount() << " followees\n";

    user1.PrintFeed();
    user2.PrintFeed();

    user1.Unfollow(&user2);
    user1.Unfollow(&user2);

    std::cout << user1.GetNickname() << " has " << user1.FollowersCount() << " followers\n";
    std::cout << user2.GetNickname() << " has " << user2.FolloweesCount() << " followees\n";

    user1.PrintFeed();
    user2.PrintFeed();

    std::cout << user3.GetNickname() << " is a transitive follower of " << user1.GetNickname() << ": "
              << std::boolalpha << IsATransitiveFollower(&user1, &user3, 3) << std::endl;

    user1.Follow(&user2);
    user1.PrintFeed();

    std::cout << user3.GetNickname() << " is a transitive follower of " << user1.GetNickname() << ": "
              << std::boolalpha << IsATransitiveFollower(&user1, &user3, 3) << std::endl;

    return 0;
}
