#include <iostream>

using namespace std;

template<typename T>
class UniquePtr {
   public:
    UniquePtr() {}
    UniquePtr(T* data) : data_(data) {}
    UniquePtr(const UniquePtr&) = delete;
    UniquePtr(UniquePtr&& other) {
        swap(other);
    }

    ~UniquePtr() {
        Reset();
    }

    UniquePtr& operator =(const UniquePtr&) = delete;
    UniquePtr& operator =(UniquePtr&& other) {
        swap(other);
        return *this;
    }

    void Reset() {
        delete data_;
        data_ = nullptr;
    }

    T& operator *() {
        return *data_;
    }

    const T& operator *() const {
        return *data_;
    }

    void swap(UniquePtr& other) {
        std::swap(this->data_, other.data_);
    }

   private:
    T* data_ = nullptr;
};

UniquePtr<int> Foo(int x) {
    return new int(x);
}

template<typename T>
auto Bar(T&& x) -> decltype(x.a + 5) {
    auto&& xx = std::forward<T&&>(x);
    xx.a = 110;
    return xx.a + 10;
}

struct S {
    int a;

    ~S() {
        cout << "Destructing S; a = " << a << endl;
    }
};

int main() {
    UniquePtr<int> pa;

    {
        UniquePtr<int> pb = new int(100);

        cout << "b = " << *pb << endl;

        pa = move(pb);
    }
    cout << "a = " << *pa << endl;

    pa = Foo(55);
    cout << "a = " << *pa << endl;

    UniquePtr<int> pc = move(pa);
    cout << "c = " << *pc << endl;

    auto f = [](int x) {return x % 2;};
    std::function<bool(int)> ff = [](int x) {return x % 2;};
    cout << "type of f " << typeid(f).name() << endl;
    cout << "type of ff " << typeid(ff).name() << endl;

    auto& pd = pc;  // будет тип const UniquePtr<int>&
    cout << "type of pd " << typeid(pd).name() << endl;

    auto& pe = const_cast<const UniquePtr<int>&>(pc);  // будет тип const UniquePtr<int>&
    // pe.Reset();  не скомпилируется из-за const типа ссылки
    cout << "type of pe " << typeid(pe).name() << endl;

    const int a = 5;
    auto aa = a;  // будет тип int
    aa = 6;  // так можно без ссылки
    cout << "type of aa " << typeid(aa).name() << endl;

    auto& aaa = a;  // будет тип const int&
    // aaa = 6;  не скомпилируется из-за const типа ссылки
    cout << "type of aaa " << typeid(aaa).name() << endl;

    auto&& a4 = a;  // будет тип const int&
    // a4 = 6;  не скомпилируется из-за const типа ссылки
    cout << "type of a4 " << typeid(a4).name() << endl;

    auto&& a5 = 5;  // будет тип
    cout << "type of a5 " << typeid(a4).name() << endl;

    // int& -- lvalue-ссылка
    // int&& -- rvalue-ссылка
    // auto&& -- универсальная ссылка

    Bar(S{5});  // T = S&&
    // Bar(const_cast<const S&&>(S{5})); не скомпилируется из-за T = const S&&
    const S ss = {5};
    // Bar(ss);  // не скомпилируется из-за T = const S&

    decltype(aaa) a6 = a;  // будет тип const int&
    decltype(5) ii = 6;  // будет тип int

    return 0;
}
