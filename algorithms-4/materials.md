# Семинар №3. 1 марта

## Link-Cut дерево

- Разобрали, как операция `expose(v)` влияет на строение splay деревьев
- Реализовали `find_root(v)`, `lca(u, v)`, `path_query(u, v)`
- Доказали асимптотику `O(log^2(N))` и `O(log(N))` для `expose(v)`

### Задачи

- [Динамическая связность](https://www.spoj.com/problems/DYNACON1/)
- [Динамические запросы на пути](https://judge.yosupo.jp/problem/dynamic_tree_vertex_add_path_sum)
- [Динамический lca](https://www.spoj.com/problems/DYNALCA/)

### Материалы

- [Лекция Маврина в CSC](https://www.youtube.com/watch?v=B6hHtQZ79b4)
- [Викиконспекты ИТМО](https://neerc.ifmo.ru/wiki/index.php?title=Link-Cut_Tree)
- [Конспект MIT](https://courses.csail.mit.edu/6.851/spring21/lectures/L19.pdf)
- [Хорошие док-ва асимптотик от MIT](https://courses.csail.mit.edu/6.851/spring21/scribe/lec19.pdf)

# Лекция №3. 22 февраля
* Link-cut tree
  * Вспоминаем по heavy-light декомпозицию
  * Структура link-cut дерева
    * Операция expose
* Доказательство асимптотики: expose работает с амортизированной оценкой `O(log^2)`
* Доказательство асимптотики: expose работает с амортизированной оценкой `O(log)`

# Семинар №2. 22 февраля

## Splay дерево

- Доказали асимптотику `O(log(N))` для `splay(x)`
- Ввели операции `join` и `split` через `splay`
- Splay дерево не хуже любого статического бинарного дерева (без перестроения между запросами) для набора запросов `M`
- Splay дерево не хуже любого динамического БД для набора запросов `M` (это не доказано и не опровергнуто)
- Splay дерево предоставляет более быстрый доступ к элементам, рядом с позицией, которую запрашивали до этого. (Т.е. при нормальном распределении запросов по позициям элементов должно работать быстрее)
- Scanning Theorem: for a splay tree that contains the values `[1, 2, . . . , n]`, accessing all of those elements in sequential order takes `O(n)` time, regardless of the initial arrangement of the tree

### Код
```cpp
struct Node {
    int sz;
    Node *p; // == nullptr if this is root
    Node *l;
    Node *r;
};

void upd(Node *x) {
    x->sz = 1;
    if (x->l) x->sz += x->l->sz;
    if (x->r) x->sz += x->r->sz;
}

// rotate x up
// Можно написать лаконичнее, если хранить детей в виде массива Node *children[2];
void rotate(Node *x) {
    assert(x->p != nullptr);
    Node *p = x->p;
    if (x->p->l == x) {
        // right move
        // Node *A = x->l;
        Node *B = x->r;
        // Node *C = p->r;

        Node *g = p->p;
        if (g) {
            if (g->l == p) g->l = x;
            else g->r = x;
        }
        x->p = g;
        // Дед is done

        p->p = x;
        p->l = B;
        if (B) B->p = p;
        x->r = p;
    } else {
        // left move
        // Node *A = p->l;
        Node *B = x->l;
        // Node *C = x->r;

        Node *g = p->p;
        if (g) {
            if (g->l == p) g->l = x;
            else g->r = x;
        }
        x->p = g;
        // Дед is done

        p->p = x;
        p->r = B;
        if (B) B->p = p;
        x->l = p;
    }
    upd(p);
    upd(x);
    if (x->p) upd(x->p);
}

void splay(Node *x) {
    while (x->p != nullptr) {
        Node *p = x->p;
        Node *g = p->p;
        if (g == nullptr) { // zig
            rotate(x);
            break;
        }
        if ((p->l == x) == (g->l == p)) { // zig-zig
            rotate(p);
            rotate(x);
        } else { // zig-zag
            rotate(x);
            rotate(x);
        }
    }
    return;
}
```

### Задачи

Splay дерево умеет решать те же задачи, что и Декартово Дерево.

- [Перемещение отрезков](https://codeforces.com/gym/102787/problem/A)
- [Проверка подстроки на палиндром](https://codeforces.com/gym/102787/problem/B)
- [Операции изменения на отрезке](https://codeforces.com/gym/102787/problem/C)

### Материалы

- [Блог Zhtluo](https://zhtluo.com/cp/splay-tree-one-tree-to-rule-them-all.html) - хорошее доказательство асимптотик операций.
- [Викиконспекты ИТМО](https://neerc.ifmo.ru/wiki/index.php?title=Splay-дерево)
- [Преза MIT](https://ocw.mit.edu/courses/6-854j-advanced-algorithms-fall-2008/921232cb9a69015c50002ff5ea6a9824_lec6.pdf) - Теоремки о сравнении с другими БД и Scanning Theorem.
- [Преза Стенфорда](https://web.stanford.edu/class/archive/cs/cs166/cs166.1146/lectures/08/Slides08.pdf) - хорошие картинки.

# Лекция №2. 15 февраля
* AWL-дерево
  * Доказательство асимптотики
* Splay-дерево
  * Операция Splay
    * Zig
    * Zig-Zig
    * Zig-Zag
  * Доказательство асимптотики - амортизированная оценка в O(1)

# Семинар №1. 15 февраля

## Персистентное ДО

### Задачи

- [Реализация перс. ДО с операциями на отрезке](https://codeforces.com/problemset/problem/707/D)
- [Кол-во чисел больше k на отрезке](https://www.spoj.com/problems/KQUERYO/)
- [Кол-во различных чисел на отрезке](https://www.spoj.com/problems/DQUERY/)
- [K-тое число на отрезке](https://www.spoj.com/problems/MKTHNUM/)
- [Объявление на заборе](https://codeforces.com/problemset/problem/484/E)
- [Пока я срываюсь](https://codeforces.com/contest/786/problem/C)

# Лекция №1. 11 февраля
* Персистентные структуры данных
  * Частичная персистентность
  * Полная персистентность
* Метод толстых узлов
* Метод копирования пути
* Применение метода копирования пути для реализации полностью персистентного дерева отрезков
  * Принцип модификации методов ДО
* Реализация частично персистетного списка с операциями с амортизированной оценкой О(1)
