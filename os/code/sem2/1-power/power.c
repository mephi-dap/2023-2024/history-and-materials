#include <stdio.h>

int power(int x, int n) {
    if (n == 0) {
        return 1;
    }
    return x * power(x, n--);
}

int main(void) {
    int base = 2;
    int exponent = 3;
    int result = power(base, exponent);
    printf("%d^%d = %d\n", base, exponent, result);
    return 0;
}