.data
strings:
    .string "Hello\n"
    .string "World\n"
    .string "Assembly\n"
    .string "Programming\n"
strings_end:

.text
.global main
main:
    # Prologue
    push %rbp
    mov %rsp, %rbp

    # Initialize counter and get start address
    lea strings(%rip), %rbx # rbx will hold current string address

.print_loop:
    # Check if we've reached the end
    lea strings_end(%rip), %rax
    cmp %rax, %rbx
    jge .done

    # Calculate string length using strlen
    mov %rbx, %rdi          # arg0: Current string
    call strlen             # Call libc strlen
    mov %rax, %rcx          # Save result to rcx for syscall

    # Use "write" syscall to print string
    mov $1, %rax            # syscall number for write
    mov $1, %rdi            # arg0: file descriptor (stdout)
    mov %rbx, %rsi          # arg1: string address
                            # arg2: string length (already in rcx)
    syscall

    # Move to next string
    add %rcx, %rbx          # Add string length to current address
    inc %rbx                # Step over null terminator
    jmp .print_loop

.done:
    # Epilogue
    mov %rbp, %rsp
    pop %rbp

    # Exit code 0
    mov $0, %rax
    ret
