.extern debug_print

.text
.global asm_fact
asm_fact:
    # Prologue
    push %rbp
    mov %rsp, %rbp
    sub $16, %rsp           # Allocate space for frame-local variables

    # Save callee-saved register rbx (to hold original n)
    mov %rbx, -8(%rbp)

    mov %rdi, %rbx          # Save n in rbx, which is callee-saved register.
                            # rdi will be clobbered by the call to debug_print
    call debug_print        # Call debug_print

    cmp $1, %rbx            # If n <= 1 then fact(n) = 1
    jle .base_case          # Jump to base case handler

    mov %rbx, %rdi          # arg0: n
    dec %rdi                # decrement n, so that arg0 is n - 1
    call asm_fact           # Call fact(n - 1)
    imul %rbx, %rax         # Multiply returned value by original n
    jmp .fact_end           # Jump over the base case handler

.base_case:
    mov $1, %rax            # Set return value to 1

.fact_end:
    # Restore callee-saved register rbx
    mov -8(%rbp), %rbx

    # Epilogue
    mov %rbp, %rsp
    pop %rbp
    ret
