#include <stdio.h>

void debug_print(long n) {
    printf("n: %ld\n", n);
}

extern long asm_fact(long n);

int main(void) {
    long n = 5;
    long result = asm_fact(n);
    printf("Factorial of %ld is %ld\n", n, result);
    return 0;
}