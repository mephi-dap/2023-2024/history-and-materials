#include <stdio.h>
#include <stdint.h>

int increment(int x) {
    __asm__ ("inc %0" : "+r"(x));
    return x;
}

int add_numbers(int a, int b) {
    int result;
    __asm__ (
        "movl %1, %0\n\t"   // Move first operand to result
        "addl %2, %0"       // Add second operand to result
        : "=r"(result)      // Output operand
        : "r"(a), "r"(b)    // Input operands
    );
    return result;
}

uint64_t get_timestamp(void) {
    uint64_t result;
    uint64_t *result_ptr = &result;
    __asm__ volatile (
        "rdtsc\n\t"             // Read time-stamp counter
        "mov %%edx, 4(%0)\n\t"  // Move EDX to high dword (offset 4)
        "mov %%eax, 0(%0)"      // Move EAX to low dword (offset 0)
        : "+r"(result_ptr)      // Output operands
        : /* No inputs */
        : "eax", "edx", "memory"    // Clobbers eax, edx and memory
    );
    return result;
}

void swap_bytes(char *a, char *b) {
    __asm__ volatile (
        "movb (%0), %%al\n\t"   // Load byte from first address into al
        "movb (%1), %%bl\n\t"   // Load byte from second address into bl
        "movb %%bl, (%0)\n\t"   // Store bl into first address
        "movb %%al, (%1)"       // Store al into second address
        : /* No outputs */
        : "r"(a), "r"(b)        // Input operands
        : "al", "bl", "memory"  // Clobbers al, bl and memory
    );
}

int main(void) {
    int x = 5;
    printf("Increment %d: %d\n", x, increment(x));

    int a = 10, b = 20;
    printf("Add %d + %d: %d\n", a, b, add_numbers(a, b));

    uint64_t ts1 = get_timestamp();
    uint64_t ts2 = get_timestamp();
    printf("Timestamps: %lu, %lu (diff: %lu)\n", ts1, ts2, ts2 - ts1);

    char str[] = "AB";
    printf("Before swap: %c%c\n", str[0], str[1]);
    swap_bytes(&str[0], &str[1]);
    printf("After swap: %c%c\n", str[0], str[1]);
}
