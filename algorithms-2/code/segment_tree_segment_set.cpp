#include <cassert>
#include <iostream>
#include <vector>
using namespace std;

class TSegmentTree
{
public:
    explicit TSegmentTree(int n)
        : N_(n)
        , Push_(4 * N_, -1)
        , Sum_(4 * N_)
    { }

    void Set(int l, int r, int x)
    {
        Set(1, 0, N_ - 1, l, r, x);
    }

    long long Get(int l, int r)
    {
        return Get(1, 0, N_ - 1, l, r);
    }

private:
    const int N_;

    vector<int> Push_;
    vector<long long> Sum_;


    void TryPush(int v, int tl, int tr)
    {
        if (tl == tr) {
            return;
        }
        if (Push_[v] == -1) {
            return;
        }
        auto x = Push_[v];
        Push_[v] = -1;

        int tm = (tl + tr) >> 1;

        Push_[2 * v] = x;
        Sum_[2 * v] = 1LL * x * (tm - tl + 1);

        Push_[2 * v + 1] = x;
        Sum_[2 * v + 1] = 1LL * x * (tr - (tm + 1) + 1);
    }

    void Set(int v, int tl, int tr, int l, int r, int x)
    {
        if (tl == l && tr == r) {
            Push_[v] = x;
            Sum_[v] = 1LL * (tr - tl + 1) * x;
        } else {
            TryPush(v, tl, tr);

            int tm = (tl + tr) >> 1;
            if (r <= tm) {
                Set(2 * v, tl, tm, l, r, x);
            } else if (l > tm) {
                Set(2 * v + 1, tm + 1, tr, l, r, x);
            } else {
                Set(2 * v, tl, tm, l, tm, x);
                Set(2 * v + 1, tm + 1, tr, tm + 1, r, x);
            }

            Sum_[v] = Sum_[2 * v] + Sum_[2 * v + 1];
        }
    }

    long long Get(int v, int tl, int tr, int l, int r)
    {
        TryPush(v, tl, tr);

        if (tl == l && tr == r) {
            return Sum_[v];
        } else {
            int tm = (tl + tr) >> 1;
            if (r <= tm) {
                return Get(2 * v, tl, tm, l, r);
            } else if (l > tm) {
                return Get(2 * v + 1, tm + 1, tr, l, r);
            } else {
                return Get(2 * v, tl, tm, l, tm) + Get(2 * v + 1, tm + 1, tr, tm + 1, r);
            }
        }
    }
};

struct TQuery
{
    char Type;
    int L;
    int R;
    int X;
};

int n, q;
vector<TQuery> qs;

void Solve()
{
    TSegmentTree st(n);
    for (auto query : qs) {
        if (query.Type == 'A') {
            st.Set(query.L, query.R, query.X);
        } else {
            cout << st.Get(query.L, query.R) << '\n';
        }
    }
    cout.flush();
}

bool Read()
{
    if (!(cin >> n >> q)) {
        return false;
    }
    qs.resize(q);
    for (int i = 0; i < q; ++i) {
        cin >> qs[i].Type;
        if (qs[i].Type == 'A') {
            cin >> qs[i].L >> qs[i].R >> qs[i].X;
            --qs[i].L;
            --qs[i].R;
        } else {
            assert(qs[i].Type == 'Q');
            cin >> qs[i].L >> qs[i].R;
            --qs[i].L;
            --qs[i].R;
        }
    }
    return true;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    freopen("input.txt", "r", stdin);
    // freopen("output.txt", "w", stdout);

    while (Read()) {
        Solve();
    }

    return 0;
}