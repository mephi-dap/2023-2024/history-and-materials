#include <cassert>
#include <iostream>
#include <random>
#include <vector>
using namespace std;

class TSegmentTree
{
public:
    explicit TSegmentTree(const vector<int>& a)
        : N_(a.size())
        , Add_(4 * N_)
        , Sum_(4 * N_)
    {
        Build(1, 0, N_ - 1, a);
    }

    void Add(int l, int r, int x)
    {
        Add(1, 0, N_ - 1, l, r, x);
    }

    long long Get(int l, int r) const
    {
        return Get(1, 0, N_ - 1, l, r);
    }

private:
    const int N_;

    vector<long long> Add_;
    vector<long long> Sum_;


    void Build(int v, int tl, int tr, const vector<int>& a)
    {
        if (tl == tr) {
            Add_[v] = a[tl];
            Sum_[v] = a[tl];
        } else {
            int tm = (tl + tr) >> 1;
            Build(2 * v, tl, tm, a);
            Build(2 * v + 1, tm + 1, tr, a);
            Sum_[v] = Sum_[2 * v] + Sum_[2 * v + 1];
        }
    }

    void Add(int v, int tl, int tr, int l, int r, int x)
    {
        Sum_[v] += 1LL * x * (r - l + 1);
        if (tl == l && tr == r) {
            Add_[v] += x;
        } else {
            int tm = (tl + tr) >> 1;
            if (r <= tm) {
                Add(2 * v, tl, tm, l, r, x);
            } else if (l > tm) {
                Add(2 * v + 1, tm + 1, tr, l, r, x);
            } else {
                Add(2 * v, tl, tm, l, tm, x);
                Add(2 * v + 1, tm + 1, tr, tm + 1, r, x);
            }
        }
    }

    long long Get(int v, int tl, int tr, int l, int r) const
    {
        if (tl == l && tr == r) {
            return Sum_[v];
        } else {
            long long result = Add_[v] * (r - l + 1);
            int tm = (tl + tr) >> 1;
            if (r <= tm) {
                result += Get(2 * v, tl, tm, l, r);
            } else if (l > tm) {
                result += Get(2 * v + 1, tm + 1, tr, l, r);
            } else {
                result += Get(2 * v, tl, tm, l, tm);
                result += Get(2 * v + 1, tm + 1, tr, tm + 1, r);
            }
            return result;
        }
    }
};

struct TQuery
{
    int Type;
    int L;
    int R;
    int X;
    int P;
};

int n;
vector<int> a;

int q;
vector<TQuery> qs;

void Solve()
{
    TSegmentTree st(a);
    for (auto query : qs) {
        if (query.Type == 1) {
            st.Add(query.L, query.R, query.X);
        } else {
            cout << st.Get(query.P, query.P) << '\n';
        }
    }
    cout.flush();
}

bool Read()
{
    if (!(cin >> n >> q)) {
        return false;
    }
    a.resize(n);
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    qs.resize(q);
    for (int i = 0; i < q; ++i) {
        cin >> qs[i].Type;
        if (qs[i].Type == 1) {
            cin >> qs[i].L >> qs[i].R >> qs[i].X;
            --qs[i].L;
            --qs[i].R;
        } else {
            assert(qs[i].Type == 2);
            cin >> qs[i].P;
            --qs[i].P;
        }
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

const int INF = 1e9;

int GetRandom(int l, int r)
{
    static mt19937 Generator(42);
    return uniform_int_distribution<int>(l, r)(Generator);
}

int TestRandomCases()
{
    for (int test = 0; test < 100; ++test) {
        int n = GetRandom(1, 10000);
        vector<int> b(n);
        for (int i = 0; i < n; ++i) {
            b[i] = GetRandom(-INF, INF);
        }
        TSegmentTree st(b);
        vector<long long> a{b.begin(), b.end()};
        for (int q = 0; q < 100000; ++q) {
            int l = GetRandom(0, n - 1);
            int r = GetRandom(l, n - 1);
            if (q & 1) {
                int x = GetRandom(-INF, INF);
                st.Add(l, r, x);
                for (int i = l; i <= r; ++i) {
                    a[i] += x;
                }
            } else {
                long long sum = st.Get(l, r);
                long long actualSum = 0;
                for (int i = l; i <= r; ++i) {
                    actualSum += a[i];
                }
                if (sum != actualSum) {
                    cout << "Failure: expected " << actualSum << ", but got " << sum << endl;
                    return 1;
                }
            }
        }
        cout << "Success " << test + 1 << endl;
    }
    return 0;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    // return TestRandomCases();

    // freopen("segmentupdate.in", "r", stdin);
    // freopen("segmentupdate.out", "w", stdout);

    while (Read()) {
        Solve();
    }

    return 0;
}
