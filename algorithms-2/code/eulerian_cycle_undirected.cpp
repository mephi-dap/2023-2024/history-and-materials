#include <iostream>
#include <vector>
#include <utility>
#include <stack>

using namespace std;

void solve() {
    int n, m;
    cin >> n >> m;
    vector<vector<pair<int, int>>> gr(n); // (vertex, edge index)
    for (int i = 0; i < m; ++i) {
        int u, v;
        cin >> u >> v;
        --u;
        --v;
        gr[u].emplace_back(v, i);
        gr[v].emplace_back(u, i);
    }

    // check
    for (int i = 0; i < n; ++i) {
        if (gr[i].size() & 1) {
            cout << "no eulerian cycle";
            return;
        }
    }

    vector<bool> deleted(m); // deleted edges
    vector<int> index(n, 0); // first not viewed edge from v

    stack<int> st;
    st.push(0);
    vector<int> cycle;
    cycle.reserve(m + 1);
    while(!st.empty()) {
        int v = st.top();
        while(index[v] < gr[v].size() && deleted[gr[v][index[v]].second]) {
            ++index[v];
        }
        // all edges are deleted
        if (index[v] == gr[v].size()) {
            cycle.push_back(v);
            st.pop();
        } else {
            // delete edge
            auto [u, id] = gr[v][index[v]];
            deleted[id] = 1;
            st.push(u);
        }
    }

    // just output cycle
    for (int v: cycle) {
        cout << v + 1 << ' ';
    }
}

int main() {
    solve();
    return 0;
}
