#pragma once

#include <optional>
#include <vector>

struct TTreapNode
{
    int Key;
    int Priority;
    int Left;
    int Right;
};

class TTreap
{
public:
    void Insert(int key);

    bool Find(int key);
    std::optional<int> FindLowerBound(int key);

private:
    std::vector<TTreapNode> Nodes_;

    int Root_ = -1;

    // <= key, > key
    std::pair<int, int> Split(int current, int key);

    int Merge(int left, int right);
};