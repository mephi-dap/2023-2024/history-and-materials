#include <vector>
#include <utility>
#include <numeric>

using namespace std;

struct DSU {
    DSU(int n): sz(n, 1), p(n, 0) {
        iota(p.begin(), p.end(), 0);
    }

    int get(int x) {
        return p[x] = (p[x] == x ? x : get(p[x]));
    }

    bool uni(int a, int b) {
        a = get(a);
        b = get(b);
        if (a == b) {
            return false;
        }
        if (sz[a] < sz[b]) {
            swap(a, b);
        }
        p[b] = a;
        sz[a] += sz[b];
        return true;
    }

private:
    vector<int> sz, p;
};

int main() {
    int n;
    // (u, v, weight)
    vector<tuple<int, int, int>> ed;

    sort(ed.begin(), ed.end(), [](const auto& l, const auto& r) {
        const auto& [lu, lv, lw] = l;
        const auto& [ru, rv, rw] = r;
        if (lw != rw) {
            return lw < rw;
        }
        return make_pair(lu, lv) < make_pair(ru, rv);
    });

    long long ans = 0;

    DSU d(n);

    for (const auto& [u, v, w]: ed) {
        if (d.uni(u, v)) {
            ans += w;
        }
    }
}



// РАБОТА НА СЕМИНАРЕ

// 1) |E| << |V|^2

// |E|log|E| + E*akkerman Kruskal win
// 


// 2) |E| << |V|^2 integer w(u, v) < |V|

// |E|*akkerman Kruskal win

// 3) |E| ~ |V|^2 
// |V|^2log|V| Prim with heap
// |V|^2 Prim


// Борувка
// (1, 2) (2, 3)

// Задача минимальный остов по произведению весов ребер
// positive weights
// 4 4 | 3 6
// П(w) -> Sum(log(w))
// MST

// Все веса различны
// Сколько различных MST
// T1 T2 SUM(T1) == SUM(T2) T3
// T1' и T2' SUM(T1') == SUM(T2')


// Дан граф G(N, M) построить дерево T, такое что максимальный вес ребра дерева минимален
// T подграф G

// Дан граф, дано MST T, q операций вида добавить ребро (u, v, w)
// u ---- v remove max(p) add (u, v, w)
// заново построить MST
// O(V)


// Макс


// Остовное дерево минимально тогда и только тогда,
// когда для каждого ребра вне дерева,
// если при его добавлении в дерево в образовавшемся цикле это ребро имеет наибольший вес
