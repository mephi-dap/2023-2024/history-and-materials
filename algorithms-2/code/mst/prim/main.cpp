#include <vector>

#include <cassert>
using namespace std;

struct TEdge
{
    int From;
    int To;
    int Weight;
};

vector<TEdge> BuildMst(const vector<vector<TEdge>>& g)
{
    const int UNDEFINED_WEIGHT = -1;

    int n = g.size();
    vector<bool> inTree(n, false);
    vector<TEdge> result;
    vector<TEdge> minEdge(n, TEdge{0, 0, UNDEFINED_WEIGHT});

    auto relax = [&] (int v) {
        inTree[v] = true;
        for (auto e : g[v]) {
            if (inTree[e.To]) {
                continue;
            }
            if (minEdge[e.To].Weight == UNDEFINED_WEIGHT || e.Weight < minEdge[e.To].Weight) {
                minEdge[e.To] = e;
            }
        }
    };

    relax(0);

    for (int i = 0; i + 1 < n; ++i) {
        int v = -1;
        for (int j = 0; j < n; ++j) {
            if (inTree[j] || minEdge[j].Weight == UNDEFINED_WEIGHT) {
                continue;
            }
            if (v == -1 || minEdge[j].Weight < minEdge[v].Weight) {
                v = j;
            }
        }
        assert(v != -1);
        result.push_back(minEdge[v]);
        relax(v);
    }
    return result;
}

int main()
{


    return 0;
}