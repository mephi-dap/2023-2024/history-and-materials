#include <queue>
#include <vector>
using namespace std;

struct TEdge
{
    int To;
    int Dist;
};

vector<vector<TEdge>> edges;

const int INF = 1e9;

int main()
{
    int n = 10;
    int start = 0;

    vector<int> dist(n, INF);

    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;

    dist[start] = 0;
    q.push(make_pair(0, start));

    while (!q.empty()) {
        auto [topDist, v] = q.top();
        q.pop();

        if (topDist > dist[v]) {
            continue;
        }

        for (auto e : edges[v]) {
            int d = dist[v] + e.Dist;
            if (dist[e.To] > d) {
                q.push(make_pair(d, e.To));
                dist[e.To] = d;
            }
        }
    }

    return 0;
}
