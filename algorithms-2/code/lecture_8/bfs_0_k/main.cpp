#include <vector>
#include <queue>
using namespace std;

struct TEdge
{
    int To;
    int Dist;
};

vector<vector<TEdge>> edges;

const int MAX_DIST = 10;
const int INF = 1e9;

int main()
{
    int n = 10;
    int start = 0;

    vector<bool> used(n);
    vector<int> dist(n, INF);

    queue<int> qs[MAX_DIST + 1];
    int currentQueue = 0;

    qs[currentQueue].push(start);
    dist[start] = 0;

    for (int iteration = 0; iteration < n * MAX_DIST; ++iteration) {
        while (!qs[currentQueue].empty()) {
            int v = qs[currentQueue].front();
            qs[currentQueue].pop();

            if (used[v]) {
                continue;
            }
            used[v] = true;

            for (auto e : edges[v]) {
                int d = dist[v] + e.Dist;
                if (dist[e.To] > d) {
                    qs[(currentQueue + e.Dist) % (MAX_DIST + 1)].push(e.To);
                    dist[e.To] = d;
                }
            }
        }
        currentQueue = (currentQueue + 1) % (MAX_DIST + 1);
    }

    return 0;
}
