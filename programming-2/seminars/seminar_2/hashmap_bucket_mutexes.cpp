#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <utility>
#include <mutex>
#include <shared_mutex>
#include <random>

template <typename K, typename V>
class HashMap {
public:
    using ItemT = std::pair<const K, V>;

    HashMap() : hashTable_(2), size_(0), bucketMutexes_(2), maxLoadFactor_(0.5f) {
    }

    ItemT* Find(const K& k) {
        std::shared_lock<std::shared_mutex> lock(tableMutex_);
        const size_t hash = std::hash<K>()(k);
        const size_t bucketIdx = hash % hashTable_.size();

        {
            std::shared_lock<std::shared_mutex> bucketLock(bucketMutexes_[bucketIdx]);

            for (auto& item : hashTable_[bucketIdx]) {
                if (item.first == k) {
                    return &item;
                }
            }
        }

        return nullptr;
    }

    void AddOrEdit(const K& k, V&& v) {
        {
            std::shared_lock<std::shared_mutex> lock(tableMutex_);

            const size_t hash = std::hash<K>()(k);
            const size_t bucketIdx = hash % hashTable_.size();

            {
                std::unique_lock<std::shared_mutex> bucketMutex(bucketMutexes_[bucketIdx]);

                for (auto& item : hashTable_[bucketIdx]) {
                    if (item.first == k) {
                        item.second = std::forward<V>(v);
                        return;
                    }
                }

                ++size_; // инкремент будет атомарным и не требует мьютексов, потому что используем std::atomic
                hashTable_[bucketIdx].push_front({k, v});
            }
        }

        // Перед вызовом Rehash снимаются блокировки, поэтому много потоков могут вставить большое количество элементов прежде чем
        // вызовется перестроение хеш-таблицы. В результате может ухудшиться время вставки и поиска в таблице, так как будет
        // большое количество элементов в бакете.
        // Пытались решить эту проблему на семинаре, но не получилось, так как все время приходили к дедлокам.
        Rehash();
    }

private:
    std::vector<std::list<ItemT>> hashTable_;
    std::atomic<size_t> size_;
    std::shared_mutex tableMutex_;
    std::vector<std::shared_mutex> bucketMutexes_;
    float maxLoadFactor_;  // https://en.cppreference.com/w/cpp/container/unordered_map/load_factor

    void Rehash() {
        std::unique_lock<std::shared_mutex> lock(tableMutex_);
        if (size_ > maxLoadFactor_ * hashTable_.size()) {
            std::vector<std::list<ItemT>> otherTable_(hashTable_.size() * 2);
            hashTable_.swap(otherTable_);

            std::vector<std::shared_mutex> newBucketMutexes(hashTable_.size());
            bucketMutexes_.swap(newBucketMutexes);

            for (const auto& bucket : otherTable_) {
                for (const auto& item : bucket) {
                    const size_t hash = std::hash<K>()(item.first);
                    const size_t bucketIdx = hash % hashTable_.size();

                    hashTable_[bucketIdx].push_front(item);
                }
            }
        }

    }
};

void PrepareNumbers(HashMap<int, int>& map, size_t amount, size_t threadIdx) {
    std::random_device rd;
    std::mt19937 random(rd());

    for (size_t i = 0; i < amount; ++i) {
        map.AddOrEdit(threadIdx * amount + i, random());
    }
}

void AddData(HashMap<int, int>& map, size_t threadIdx) {
    PrepareNumbers(map, 100000, threadIdx);
}

void ReadData(HashMap<int, int>& map, size_t maxKey) {
    std::vector<HashMap<int, int>::ItemT> values;

    for (size_t i = 0; i < maxKey; ++i) {
        if (auto it = map.Find(i)) {
            values.push_back(*it);
        }
    }
}

int main() {
    return 0;
}
