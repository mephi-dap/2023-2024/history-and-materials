#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <utility>
#include <mutex>
#include <shared_mutex>
#include <random>

template <typename K, typename V>
class HashMap {
public:
    using ItemT = std::pair<const K, V>;

    HashMap() : hashTable_(2), nonEmptyBucketsCount_(0) {
    }

    ItemT* Find(const K& k) {
        std::shared_lock<std::shared_mutex> lock(tableMutex_);
        const size_t hash = std::hash<K>()(k);
        const size_t bucketIdx = hash % hashTable_.size();

        for (auto& item : hashTable_[bucketIdx]) {
            if (item.first == k) {
                return &item;
            }
        }

        return nullptr;
    }

    void AddOrEdit(const K& k, V&& v) {
        std::unique_lock<std::shared_mutex> lock(tableMutex_);

        const size_t hash = std::hash<K>()(k);
        const size_t bucketIdx = hash % hashTable_.size();

        for (auto& item : hashTable_[bucketIdx]) {
            if (item.first == k) {
                item.second = std::forward<V>(v);
                return;
            }
        }

        if (hashTable_[bucketIdx].empty()) {
            ++nonEmptyBucketsCount_;
        }

        hashTable_[bucketIdx].push_front({k, v});

        Rehash();
    }

private:
    std::vector<std::list<ItemT>> hashTable_;
    size_t nonEmptyBucketsCount_;
    std::shared_mutex tableMutex_;

    void Rehash() {
        if ((float)nonEmptyBucketsCount_ / hashTable_.size() > 0.5f) {  // ToDo: fixme
            std::vector<std::list<ItemT>> otherTable_(hashTable_.size() * 2);
            std::swap(hashTable_, otherTable_);
            nonEmptyBucketsCount_ = 0;

            for (const auto& bucket : otherTable_) {
                for (const auto& item : bucket) {
                    const size_t hash = std::hash<K>()(item.first);
                    const size_t bucketIdx = hash % hashTable_.size();

                    if (hashTable_[bucketIdx].empty()) {
                        ++nonEmptyBucketsCount_;
                    }

                    hashTable_[bucketIdx].push_front(item);
                }
            }
        }

    }
};

void PrepareNumbers(HashMap<int, int>& map, size_t amount, size_t threadIdx) {
    std::random_device rd;
    std::mt19937 random(rd());

    for (size_t i = 0; i < amount; ++i) {
        map.AddOrEdit(threadIdx * amount + i, random());
    }
}

void AddData(HashMap<int, int>& map, size_t threadIdx) {
    PrepareNumbers(map, 100000, threadIdx);
}

void ReadData(HashMap<int, int>& map, size_t maxKey) {
    std::vector<HashMap<int, int>::ItemT> values;

    for (size_t i = 0; i < maxKey; ++i) {
        if (auto it = map.Find(i)) {
            values.push_back(*it);
        }
    }
}

int main() {
    return 0;
}
