from functools import wraps


def count_calls(func):
    print(f"Decorating function {func.__name__}")

    assert not hasattr(func, "calls_count")

    @wraps(func)  # затираем __name__, чтобы снаружи было не понятно, что мы вернули другую функцию
    def decorated(*args, **kwargs):
        decorated.calls_count += 1
        return func(*args, **kwargs)

    decorated.calls_count = 0

    return decorated


def add_suffix_to_name(suffix: str):
    def decorator(func):
        func.__name__ = func.__name__ + suffix
        return func

    return decorator


decorators = [add_suffix_to_name]


@decorators[0]("_xxxxx")  # после @ может быть любое выражение, возвращающее декоратор
@count_calls
def foo(x, y):
    return x + y


"""
Одно и то же
def foo(x, y):
    return x + y

foo = count_calls(foo)
"""


@count_calls
@add_suffix_to_name("_yyyyy")
def bar(x, y):
    return x - y


def baz(x, *, y, **kwargs):
    print(x)


def print_geo_point(*, latitude, longitude):  # * не дает передать latitude и longitude как позиционные аргументы
    print(f"lat: {latitude}, lon: {longitude}")


def minimum(a, b, /):  # / не дает передать a и b как key-value аргументы
    return min(a, b)


def printable(cls):
    def print_name(self):
        print(f"class name is {self.__class__.__name__}")

    assert not hasattr(cls, "print_name")
    cls.print_name = print_name

    return cls


def change_print_name(cls):
    assert hasattr(cls, "print_name")

    class WrappedClass(cls):
        def print_name(self):
            print(f"New print name!!! {self.__class__.__name__}")

    WrappedClass.__name__ = cls.__name__

    return WrappedClass


@printable
class Foo:
    pass


@change_print_name
@printable
class Bar:
    def other_method(self, x):
        print(f"Other method called {x}")


class Base:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def print_something(self):
        print("Base.print_something")


class BaseMixin:
    def print_something(self):
        print("BaseMixin.print_something")


class Derived(BaseMixin, Base):
    def __init__(self, a, b):
        super().__init__(a, b)

    def print_something(self):
        super().print_something()
        print("Derived.print_something")


def main():
    print(f"name of foo: {foo.__name__}")
    print(f"foo called {foo.calls_count} times")
    foo(1, 2)
    foo(3, 4)
    foo(4, 5)
    print(f"foo called {foo.calls_count} times")

    print(f"bar called {bar.calls_count} times")
    bar(x=1, y=2)
    bar(3, y=4)
    bar(4, 5)
    print(f"bar called {bar.calls_count} times")

    foo_instance = Foo()
    foo_instance.print_name()
    print(type(foo_instance))
    print(type(Foo))
    print(foo_instance.__class__)
    assert foo_instance.__class__ is Foo

    bar_instance = Bar()
    bar_instance.print_name()
    print(type(bar_instance))
    print(type(Bar))
    print(bar_instance.__class__)
    assert bar_instance.__class__ is Bar
    bar_instance.other_method(1555)

    d = Derived(1, 2)
    d.print_something()


if __name__ == '__main__':
    main()
