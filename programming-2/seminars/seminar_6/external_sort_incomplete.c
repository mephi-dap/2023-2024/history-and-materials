#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>

#define min(a, b) ((a) < (b) ? (a) : (b))

const size_t ramBytes = 100;
const size_t elemSize = sizeof(int);
const size_t elemsCount = ramBytes / elemSize;
int ramData[elemsCount];

int PrintError() {
    if (errno == EACCES) {
        char buf[] = "Error: no access";
        write(2, buf, sizeof(buf));
        return -1;
    } else {
        char buf[] = "Error: ";
        write(2, buf, sizeof(buf));

        char errorBuf[128];
        perror(errorBuf);
        write(2, errorBuf, strlen(errorBuf));

        return -1;
    }
}

int ReadIntoMemory(const int fd, int begin, int end) {
    const int bytesToRead = (end - begin) * (int)elemSize;
    if (lseek(fd, begin * (off_t)elemSize, SEEK_SET) == -1) {
        return PrintError();
    }
    if (read(fd, ramData, bytesToRead) == -1) {
        return PrintError();
    }
}

int CompareIntegers(const void* lhs, const void* rhs) {
    const int l = *((int*)lhs);
    const int r = *((int*)rhs);
    return l - r;
}

int MemorySort(const int fd, int begin, int end) {
    ReadIntoMemory(fd, begin, end);
    qsort(ramData, end - begin, elemSize, CompareIntegers);

    if (lseek(fd, begin * (off_t)elemSize, SEEK_SET) == -1) {
        return PrintError();
    }
    const int bytesCount = (end - begin) * (int)elemSize;
    if (write(fd, ramData, bytesCount) == -1) {
        return PrintError();
    }

    return 0;
}

int MergeSort(const int fd, int begin, int end) {
    if (end - begin <= elemsCount) {
        return MemorySort(fd, begin, end);
    }

    const int m = (begin + end) / 2;
    if (MergeSort(fd, begin, m) == -1) {
        return -1;
    }
    if (MergeSort(fd, m, end) == -1) {
        return -1;
    }

//    return Merge(fd, begin, end);
// ToDo: кто сделает до вторника 26.03, получит плюс балл
// Если будет много решений, то наиболее эффективное с точки зрения количества операций с файлами получит балл
}

int main() {
    const int fd = open("sorting.int", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fd == -1 ) {
        return PrintError();
    }

    int numbersCount = 1000000;
    for (int i = numbersCount; i > 0; --i) {
        if (write(fd, &i, sizeof(int)) == -1) {
            return PrintError();
        }
    }

    ReadIntoMemory(fd, 0, elemsCount);

    for (int i = 0; i < elemsCount; ++i) {
        printf("%d ", ramData[i]);
    }

    return 0;
}
