#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define min(a, b) ((a) < (b) ? (a) : (b))

#define CHECK(expr)              \
    do {                         \
        if ((expr) == -1) {      \
            return PrintError(); \
        }                        \
    } while (false)

#define CHECK_NO_PRINT(expr) \
    do {                     \
        if ((expr) == -1) {  \
            return -1;       \
        }                    \
    } while (false)

const size_t ramBytes = 128;
const size_t elemSize = sizeof(int);
const size_t elemsCount = ramBytes / elemSize;
int ramData[elemsCount];

int PrintError() {
    if (errno == EACCES) {
        char buf[] = "Error: no access";
        write(2, buf, sizeof(buf));
    } else {
        char buf[] = "Error: ";
        write(2, buf, sizeof(buf));

        char errorBuf[128];
        perror(errorBuf);
        write(2, errorBuf, strlen(errorBuf));
    }

    return -1;
}

int ReadIntoMemory(const int fd, int begin, int end) {
    const int bytesToRead = (end - begin) * (int)elemSize;
    CHECK(lseek(fd, begin * (off_t)elemSize, SEEK_SET));
    CHECK(read(fd, ramData, bytesToRead));
    return 0;
}

int CompareIntegers(const void* lhs, const void* rhs) {
    const int l = *((int*)lhs);
    const int r = *((int*)rhs);
    return l - r;
}

int MemorySort(const int fd, int begin, int end) {
    CHECK_NO_PRINT(ReadIntoMemory(fd, begin, end));
    qsort(ramData, end - begin, elemSize, CompareIntegers);

    CHECK(lseek(fd, begin * (off_t)elemSize, SEEK_SET));
    const int bytesCount = (end - begin) * (int)elemSize;
    CHECK(write(fd, ramData, bytesCount));

    return 0;
}

int Merge(const int fd, const int tempBufFd, int begin, int end) {
    CHECK(lseek(tempBufFd, 0, SEEK_SET));

    const int m = (begin + end) / 2;
    for (int i = begin, j = m; i < m || j < end;) {
        int a = 0, b = 0;
        if (i < m) {
            CHECK(lseek(fd, i * (off_t)elemSize, SEEK_SET));
            CHECK(read(fd, &a, elemSize));
        }
        if (j < end) {
            CHECK(lseek(fd, j * (off_t)elemSize, SEEK_SET));
            CHECK(read(fd, &b, elemSize));
        }

        if (j >= end || (i < m && CompareIntegers(&a, &b) < 0)) {
            CHECK(write(tempBufFd, &a, elemSize));
            i++;
        } else {
            CHECK(write(tempBufFd, &b, elemSize));
            j++;
        }
    }

    CHECK(lseek(tempBufFd, 0, SEEK_SET));
    CHECK(lseek(fd, begin * (off_t)elemSize, SEEK_SET));
    int count = end - begin;
    while (count) {
        ssize_t readBytes = read(tempBufFd, ramData, min(count, elemsCount) * elemSize);
        CHECK(readBytes);
        count -= readBytes / elemSize;
        CHECK(write(fd, ramData, readBytes));
    }
    return 0;
}

int MergeSortImpl(const int fd, const int tempBufFd, int begin, int end) {
    if (end - begin <= elemsCount) {
        return MemorySort(fd, begin, end);
    }

    const int m = (begin + end) / 2;
    CHECK_NO_PRINT(MergeSortImpl(fd, tempBufFd, begin, m));
    CHECK_NO_PRINT(MergeSortImpl(fd, tempBufFd, m, end));
    return Merge(fd, tempBufFd, begin, end);
}

int MergeSort(const int fd, int count) {
    const int tempBufFd = open("temp.int", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    CHECK(tempBufFd);
    int res = MergeSortImpl(fd, tempBufFd, 0, count);
    CHECK(close(tempBufFd));
    return res;
}

int main() {
    const int fd = open("sorting.int", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    CHECK(fd);

    int numbersCount = 1000000;
    for (int i = numbersCount; i > 0; --i) {
        CHECK(write(fd, &i, sizeof(int)));
    }

    CHECK_NO_PRINT(ReadIntoMemory(fd, 0, elemsCount));

    for (int i = 0; i < elemsCount; ++i) {
        printf("%d ", ramData[i]);
    }
    printf("\n");

    CHECK(lseek(fd, 0, SEEK_SET));
    CHECK_NO_PRINT(MergeSort(fd, numbersCount));

    CHECK_NO_PRINT(ReadIntoMemory(fd, 0, elemsCount));

    for (int i = 0; i < elemsCount; ++i) {
        printf("%d ", ramData[i]);
    }

    CHECK(close(fd));
    return 0;
}
