#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>


#define min(a, b) ((a) < (b) ? (a) : (b))

#define CHECK(expr)		 	 \
	do {						 \
	    if ((expr) == -1) {	  \
	        return PrintError(); \
	    }                        \
	} while(0)

#define CHECK_NO_PRINT(expr) \
	do { 				    \
	    if ((expr) == -1) {  \
	        return -1;       \
	    }                    \
	} while(0)

const size_t ramBytes = 31 * 4;
const size_t elemSize = sizeof(int);
const size_t elemsCount = ramBytes / elemSize;
int ramData[elemsCount];


struct Queue
{
    int b, e;
    int* data;
    int maxSize;
};

int Size(const struct Queue* q)
{
    return q->e - q->b;
}

void Pop(struct Queue* q)
{
    q->b++;
}

int Front(struct Queue* q)
{
    return q->data[q->b];
}

void Push(struct Queue* q, int v)
{
    q->data[q->e++] = v;
}


int PrintError() {
    if (errno == EACCES) {
        char buf[] = "Error: no access";
        write(2, buf, sizeof(buf));
    }
    else {
        char buf[] = "Error: ";
        write(2, buf, sizeof(buf));

        char errorBuf[128];
        perror(errorBuf);
        write(2, errorBuf, strlen(errorBuf));
    }

    return -1;
}

int ReadIntoMemory(const int fd, int begin, int end) {
    const int bytesToRead = (end - begin) * (int)elemSize;
    CHECK(lseek(fd, begin * (off_t)elemSize, SEEK_SET));
    CHECK(read(fd, ramData, bytesToRead));
    return 0;
}

int CompareIntegers(const void* lhs, const void* rhs) {
    const int l = *((int*)lhs);
    const int r = *((int*)rhs);
    return l - r;
}

int MemorySort(const int fd, int begin, int end) {
    CHECK_NO_PRINT(ReadIntoMemory(fd, begin, end));
    qsort(ramData, end - begin, elemSize, CompareIntegers);

    CHECK(lseek(fd, begin * (off_t)elemSize, SEEK_SET));
    const int bytesCount = (end - begin) * (int)elemSize;
    CHECK(write(fd, ramData, bytesCount));

    return 0;
}

int Merge(const int fd, const int tempBufFd, int begin, int end) {
    static const size_t partElemsCount = elemsCount / 3;
    const int m = (begin + end) / 2;

    CHECK(lseek(tempBufFd, 0, SEEK_SET));

    int i = begin, j = m;
    struct Queue q1 = { 0, 0, ramData, partElemsCount };
    struct Queue q2 = { 0, 0, ramData + partElemsCount, partElemsCount };
    struct Queue q3 = { 0, 0, ramData + 2 * partElemsCount, elemsCount - 2 * partElemsCount };
    
    while (i < m || j < end || Size(&q1) > 0 || Size(&q2) > 0)
    {
        if (i < m && Size(&q1) == 0) {
            CHECK(lseek(fd, i * (off_t)elemSize, SEEK_SET));
            ssize_t bytesRead = read(fd, q1.data, min(m - i, q1.maxSize) * elemSize);
            CHECK(bytesRead);
            i += bytesRead / elemSize;
            q1.b = 0;
            q1.e = bytesRead / elemSize;
        }
        if (j < end && Size(&q2) == 0) {
            CHECK(lseek(fd, j * (off_t)elemSize, SEEK_SET));
            ssize_t bytesRead = read(fd, q2.data, min(end - j, q2.maxSize) * elemSize);
            CHECK(bytesRead);
            j += bytesRead / elemSize;
            q2.b = 0;
            q2.e = bytesRead / elemSize;
        }

        while (Size(&q3) < q3.maxSize 		  &&
               (Size(&q1) > 0 || Size(&q2) > 0) &&
        	   !(i < m && Size(&q1) == 0) 	  &&
        	   !(j < end && Size(&q2) == 0)
        )
        {
            int a = (Size(&q1) == 0 ? INT_MAX : Front(&q1));
            int b = (Size(&q2) == 0 ? INT_MAX : Front(&q2));
            if ((Size(&q2) == 0) || (Size(&q1) > 0 && CompareIntegers(&a, &b) < 0)) { Pop(&q1); Push(&q3, a); }
            else { Pop(&q2); Push(&q3, b); }
        }

        if (Size(&q3) == q3.maxSize)
        {
            CHECK(write(tempBufFd, q3.data, Size(&q3) * elemSize));
            q3.b = q3.e = 0;
        }
    }

    if (Size(&q3) != 0)
    {
        CHECK(write(tempBufFd, q3.data, Size(&q3) * elemSize));
        q3.b = q3.e = 0;
    }
    
    CHECK(lseek(tempBufFd, 0, SEEK_SET));
    CHECK(lseek(fd, begin * (off_t)elemSize, SEEK_SET));
    int count = end - begin;
    while (count)
    {
        ssize_t readBytes = read(tempBufFd, ramData, min(count, elemsCount) * elemSize);
        CHECK(readBytes);
        count -= readBytes / elemSize;
        CHECK(write(fd, ramData, readBytes));
    }
    
    return 0;
}

int MergeSortImpl(const int fd, const int tempBufFd, int begin, int end) {
    if (end - begin <= elemsCount) {
        return MemorySort(fd, begin, end);
    }

    const int m = (begin + end) / 2;
    CHECK_NO_PRINT(MergeSortImpl(fd, tempBufFd, begin, m));
    CHECK_NO_PRINT(MergeSortImpl(fd, tempBufFd, m, end));
    return Merge(fd, tempBufFd, begin, end);
}

int MergeSort(const int fd, int count)
{
    const int tempBufFd = open("temp.int", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    CHECK(tempBufFd);
    int res = MergeSortImpl(fd, tempBufFd, 0, count);
    CHECK(close(tempBufFd));
    return res;
}

int main() {
    const int fd = open("sorting.int", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    CHECK(fd);

    int numbersCount = 1000000;
    for (int i = numbersCount; i > 0; --i) {
        int v = i;
        CHECK(write(fd, &v, sizeof(int)));
    }

    CHECK_NO_PRINT(ReadIntoMemory(fd, 0, elemsCount));

    for (int i = 0; i < elemsCount; ++i) {
        printf("%d ", ramData[i]);
    }
    printf("\n");

    CHECK(lseek(fd, 0, SEEK_SET));
	CHECK_NO_PRINT(MergeSort(fd, numbersCount));

    CHECK_NO_PRINT(ReadIntoMemory(fd, 0, elemsCount));
    for (int i = 0; i < elemsCount; ++i) {
        printf("%d ", ramData[i]);
    }

    CHECK(close(fd));
    return 0;
}
