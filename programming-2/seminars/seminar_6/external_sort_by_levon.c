#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const size_t ramLimitBytes = 100;
const size_t elementSize = sizeof(int);
const size_t ramLimitIntegers = ramLimitBytes / elementSize;

int ramData[ramLimitIntegers];

void PrintError() {
    char errorBuf[128];
    perror(errorBuf);
    write(1, errorBuf, strlen(errorBuf));
}

int CompareIntegers(const void* lhs, const void* rhs) {
    const int l = *((int*)lhs);
    const int r = *((int*)rhs);
    if (l < r) {
        return -1;
    }
    if (l > r) {
        return 1;
    }
    return 0;
}

size_t min(size_t lhs, size_t rhs) {
    if (lhs < rhs) {
        return lhs;
    }
    return rhs;
}

int SortInMemory(int fd, int beginPos, int endPos) {
    const size_t dataSize = min(endPos - beginPos, ramLimitIntegers);

    if (lseek(fd, beginPos * elementSize, SEEK_SET) == -1) {
        return -1;
    }
    if (read(fd, ramData, dataSize * elementSize) == -1) {
        return -1;
    }

    qsort(ramData, dataSize, elementSize, CompareIntegers);

    if (lseek(fd, beginPos * elementSize, SEEK_SET) == -1) {
        return -1;
    }
    if (write(fd, ramData, dataSize * elementSize) == -1) {
        return -1;
    }

    return 0;
}

int ReadElementsIntoRam(int fd, int* startPositionInFile, int* readTo, size_t maxElements) {
    if (lseek(fd, *startPositionInFile * elementSize, SEEK_SET) == -1) {
        return -1;
    }

    const size_t bytesRead = read(fd, readTo, maxElements * elementSize);
    if (bytesRead == -1) {
        return -1;
    }

    *startPositionInFile += bytesRead / elementSize;

    return bytesRead / elementSize;
}

int CopyFile(int fromFD, int toFD, int fromPosition, int toPosition, size_t elementsCount) {
    int elementsCopiedTotal = 0;

    if (lseek(toFD, toPosition * elementSize, SEEK_SET) == -1) {
        return -1;
    }

    while (elementsCount > 0) {
        int elementsCopied = ReadElementsIntoRam(fromFD, &fromPosition, ramData, min(ramLimitIntegers, elementsCount));
        if (elementsCopied == -1) {
            return -1;
        }
        elementsCopiedTotal += elementsCopied;
        elementsCount -= elementsCopied;

        if (write(toFD, ramData, elementsCopied * elementSize) == -1) {
            return -1;
        }
    }

    return elementsCopiedTotal;
}

int Merge(int fd, int beginPos, int endPos, int m) {
    const int bufferFD = open("mergeBuffer.int", O_RDWR | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR);

    int* lhsDataEnd = ramData + (ramLimitIntegers / 2);
    int* lhsData = lhsDataEnd;
    int* rhsDataEnd = &ramData[ramLimitIntegers];
    int* rhsData = rhsDataEnd;

    size_t leftElementsToProcess = m - beginPos;
    size_t rightElementsToProcess = endPos - m;

    int leftPositionInFile = beginPos;
    int rightPositionInFile = m;

    while (leftElementsToProcess > 0 && rightElementsToProcess > 0) {
        if (lhsData == lhsDataEnd) {
            lhsData = ramData;
            if (ReadElementsIntoRam(fd, &leftPositionInFile, lhsData, min(leftElementsToProcess, ramLimitIntegers / 2)) == -1) {
                return -1;
            }
        }
        if (rhsData == rhsDataEnd) {
            rhsData = &ramData[ramLimitIntegers / 2];
            if (ReadElementsIntoRam(fd, &rightPositionInFile, rhsData, min(rightElementsToProcess, ramLimitIntegers - ramLimitIntegers / 2)) == -1) {
                return -1;
            }
        }

        const int compResult = CompareIntegers(lhsData, rhsData);
        if (compResult == -1 || compResult == 0) {
            if (write(bufferFD, lhsData, elementSize) == -1) {
                return -1;
            }
            ++lhsData;
            --leftElementsToProcess;
        } else {
            if (write(bufferFD, rhsData, elementSize) == -1) {
                return -1;
            }
            ++rhsData;
            --rightElementsToProcess;
        }
    }

    while (leftElementsToProcess > 0) {
        if (lhsData == lhsDataEnd) {
            lhsData = ramData;
            if (ReadElementsIntoRam(fd, &leftPositionInFile, lhsData, min(leftElementsToProcess, ramLimitIntegers / 2)) == -1) {
                return -1;
            }
        }

        if (write(bufferFD, lhsData, elementSize) == -1) {
            return -1;
        }
        ++lhsData;
        --leftElementsToProcess;
    }

    while (rightElementsToProcess > 0) {
        if (rhsData == rhsDataEnd) {
            rhsData = &ramData[ramLimitIntegers / 2];
            if (ReadElementsIntoRam(fd, &rightPositionInFile, rhsData, min(rightElementsToProcess, ramLimitIntegers / 2)) == -1) {
                return -1;
            }
        }

        if (write(bufferFD, rhsData, elementSize) == -1) {
            return -1;
        }
        ++rhsData;
        --rightElementsToProcess;
    }

    if (CopyFile(bufferFD, fd, 0, beginPos, endPos - beginPos) == -1) {
        return -1;
    }

    close(bufferFD);
    return 0;
}

int MergeSort(int fd, int beginPos, int endPos) {
    if (endPos - beginPos <= ramLimitIntegers) {
        return SortInMemory(fd, beginPos, endPos);
    }

    const int m = (beginPos + endPos) / 2;
    if (MergeSort(fd, beginPos, m) == -1) {
        return -1;
    }
    if (MergeSort(fd, m, endPos) == -1) {
        return -1;
    }

    if (Merge(fd, beginPos, endPos, m) == -1) {
        return -1;
    }

    return 0;
}

int main() {
    const int testFD = open("sorting.int", O_CREAT | O_TRUNC | O_RDWR, S_IRUSR | S_IWUSR);
    if (testFD == -1) {
        PrintError();
        return -1;
    }

    int numbersCount = 1000000;
    for (int i = numbersCount; i > 0; --i) {
        write(testFD, &i, sizeof(i));
    }

    if (MergeSort(testFD, 0, numbersCount) == -1) {
        printf("MergeSort failed:\n");
        PrintError();
        close(testFD);
        return -1;
    }

    int pos = 0;
    if (ReadElementsIntoRam(testFD, &pos, ramData, ramLimitIntegers) == -1) {
        printf("Read failed:\n");
        PrintError();
        close(testFD);
        return -1;
    }

    for (int i = 0; i < pos; ++i) {
        printf("%d ", ramData[i]);
    }

    close(testFD);

    return 0;
}
