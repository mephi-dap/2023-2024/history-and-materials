#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#define min(a, b) (a < b ? a : b)

int PrintError() {
    if (errno == EACCES) {
        char buf[] = "Error: no access";
        write(2, buf, sizeof(buf));
        return 1;
    } else {
        char buf[] = "Error: ";
        write(2, buf, sizeof(buf));

        char errorBuf[128];
        perror(errorBuf);
        write(2, errorBuf, strlen(errorBuf));

        return 2;
    }
}

int main() {
    const int outputFD = open("output.txt", O_WRONLY | O_TRUNC | O_CREAT, S_IWUSR | S_IRUSR);
    if (outputFD == -1) {
        return PrintError();
    }

    const int randomFD = open("/dev/urandom", O_RDONLY);
    if (randomFD == -1) {
        close(outputFD);  // не обязательно, так как ОС сама освобождает ресурсы после завершения процесса
        return PrintError();
    }

    char buf[128];
    int toRead = 1000000;

    while (toRead) {
        const int wantToReadForIteration = min(sizeof(buf), toRead);
        const ssize_t bytesRead = read(randomFD, buf, wantToReadForIteration);
        if (bytesRead == -1) {
            return PrintError();
        }
        if (write(outputFD, buf, bytesRead) == -1) {
            return PrintError();
        }
        toRead -= bytesRead;
    }

    return 0;
}
