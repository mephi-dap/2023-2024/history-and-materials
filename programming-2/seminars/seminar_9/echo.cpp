#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <thread>
#include <unistd.h>
#include <vector>
#include <queue>
#include <condition_variable>
#include <stdexcept>
#include <cassert>
#include <poll.h>
#include <iostream>
#include <unordered_set>
#include <future>
#include <algorithm>

using namespace std::chrono_literals;

class ThreadPool {
public:
    ThreadPool(size_t threadCount) : Terminated_(0) {
        std::lock_guard lock(Mutex_);

        for(size_t i = 0; i < threadCount; ++i) {
            Threads_.emplace_back(
                    [&] () {
                        while(true) {
                            std::unique_lock lock(Mutex_);

                            TasksPushed_.wait(lock, [&](){return !TasksQueue_.empty() || Terminated_ != 0;});
                            // wait while empty queue and not terminated

                            if (Terminated_ == 1 && TasksQueue_.empty()) {
                                EmptyQueue_.notify_one();
                                break;
                            }
                            if (Terminated_ == 2) {
                                break;
                            }

                            std::function<void()> cur_task = TasksQueue_.front();
                            TasksQueue_.pop();
                            lock.unlock();
                            cur_task();
                        }
                        return;
                    }
            );
        }
    }
    ThreadPool(const ThreadPool&) = delete;

    void PushTask(const std::function<void()>& task) {
        std::lock_guard lock(Mutex_);
        if (Terminated_ == 0) {
            TasksQueue_.push(task);
            TasksPushed_.notify_one();
        }
        else {
            throw std::runtime_error("push task to terminated pool");
        }
    }

    void Terminate(bool wait) {
        std::unique_lock lock(Mutex_);

        if (Terminated_ != 0) {
            throw std::runtime_error("pool is already terminated");
        }

        if (wait) {
            Terminated_ = 1;
            EmptyQueue_.wait(lock, [&](){return TasksQueue_.empty();});
            // now the queue is empty
        }

        // no waiting or queue is empty
        Terminated_ = 2;

        lock.unlock();

        TasksPushed_.notify_all();

        for (std::thread& thread : Threads_) {
            thread.join();
        }

        return;
    }

    bool IsActive() const {
        std::lock_guard lock(Mutex_);
        return !Terminated_;
    }

    size_t QueueSize() const {
        std::lock_guard lock(Mutex_);
        return TasksQueue_.size();
    }

private:
    mutable std::mutex Mutex_;
    char Terminated_;  // 0 - not terminated, 1 - terminated and wait, 2 - terminated and don't wait

    std::vector<std::thread> Threads_;
    std::queue<std::function<void()>> TasksQueue_;

    std::condition_variable TasksPushed_;
    std::condition_variable EmptyQueue_;
};

void ProcessData(int fd, std::shared_ptr<std::promise<void>> p) {
    // ToDo: что если будет исключение? Надо вызывать set_value в любом случае
    char buffer[1000];
    ssize_t bytesReceived;

    if ((bytesReceived = recv(fd, &buffer, sizeof(buffer), 0)) == -1) {
        perror("recv");
        p->set_value();
        return;
    }
    std::cout << "Received from " << fd << std::endl;

    if (send(fd, buffer, bytesReceived, 0) == -1) {
        perror("send");
        p->set_value();
        return;
    }

    p->set_value();
}

void PollSockets(std::vector<int>& fds, std::mutex& mutex, ThreadPool& pool) {
    while (true) {
        std::unique_lock<std::mutex> lock(mutex);

        const size_t fdsCount = fds.size();
        pollfd pollFds[fdsCount];
        for (size_t i = 0; i < fdsCount; ++i) {
            pollFds[i] = {fds[i], POLLIN};
        }

        lock.unlock();

        int pollResult = poll(pollFds, 1, 5000);

        if (pollResult == 0) {
            std::cout << "Nothing to read" << std::endl;
            continue;
        }

        if (pollResult == -1) {
            perror("poll");
            return;
        }

        std::unordered_set<int> fdsToRemove;
        std::vector<std::future<void>> waitingRead;
        for (size_t i = 0; i < fdsCount; ++i) {
            if (pollFds[i].revents & POLLHUP) {
                if (close(pollFds[i].fd) == -1) {
                    perror("close");
                }
                std::cout << "Closing " << pollFds[i].fd << std::endl;
                fdsToRemove.insert(pollFds[i].fd);
            } else if (pollFds[i].revents & POLLIN) {
                const int fd = pollFds[i].fd;
                auto promise = std::make_shared<std::promise<void>>();
                waitingRead.push_back(promise->get_future());
                pool.PushTask([fd, promise](){ProcessData(fd, promise);});
            }
        }

        while (!std::all_of(waitingRead.begin(), waitingRead.end(),
                           [](const std::future<void>& f) {return f.wait_for(10ms) == std::future_status::ready;})) {}

        lock.lock();
        const auto removeFrom = std::remove_if(fds.begin(), fds.end(), [&fdsToRemove](int fd){return fdsToRemove.contains(fd);});
        fds.resize(removeFrom - fds.begin());
    }

}

int main() {
    int socketFd = socket(PF_INET, SOCK_STREAM, 0);
    if (socketFd == -1) {
        perror("Socket");
        return -1;
    }

    struct sockaddr_in address;

    memset(&address, 0, sizeof(struct sockaddr_in));

    address.sin_family = PF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(8080);

    if (bind(socketFd, (sockaddr*)&address, sizeof(sockaddr_in)) == -1) {
        perror("Bind");
        return -1;
    }

    if (listen(socketFd, 5) == -1) {
        perror("Listen");
        return -1;
    }

    std::vector<int> fds;
    std::mutex fdsMutex;
    ThreadPool pool(10);

    std::thread pollThread(PollSockets, std::ref(fds), std::ref(fdsMutex), std::ref(pool));

    // ToDo: предусмотреть механизм graceful shutdown, чтобы выходить из цикла, отвечать на все оставшиеся запросы и завершаться
    while (true) {
        struct sockaddr_in incomingAddress;
        socklen_t addressLen;
        int incomeFd;
        if ((incomeFd = accept(socketFd, (sockaddr*)&incomingAddress, &addressLen)) == -1) {
            perror("Accept");
            return -1;
        }
        // ToDo: обернуть файловый дескриптор сокета в RAII класс, чтобы сокеты гарантированно закрывались и не надо было за этим следить
        std::lock_guard<std::mutex> fdsLock(fdsMutex);
        fds.push_back(incomeFd);
    }

    pollThread.join();
    pool.Terminate(true);

    return 0;
}
