#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <utility>

template <typename K, typename V>
class HashMap {
public:
    using ItemT = std::pair<const K, V>;

    HashMap() : hashTable_(2), nonEmptyBucketsCount_(0) {
    }

    std::optional<ItemT&> Find(const K& k) {
        const size_t hash = std::hash<K>(k);
        const size_t bucketIdx = hash % hashTable_.size();

        for (auto& item : hashTable_[bucketIdx]) {
            if (item.first == k) {
                return item;
            }
        }

        return std::nullopt;
    }

    void AddOrEdit(const K& k, V&& v) {
        const size_t hash = std::hash<K>(k);
        const size_t bucketIdx = hash % hashTable_.size();

        for (auto& item : hashTable_[bucketIdx]) {
            if (item.first == k) {
                item.second = std::forward<V>(v);
                return;
            }
        }

        if (hashTable_[bucketIdx].empty()) {
            ++nonEmptyBucketsCount_;
        }

        hashTable_[bucketIdx].push_front({k, v});

        if ((float)nonEmptyBucketsCount_ / hashTable_.size() > 0.5f) {
            Rehash();
        }
    }

private:
    std::vector<std::list<ItemT>> hashTable_;
    size_t nonEmptyBucketsCount_;

    void Rehash() {
        std::vector<std::list<ItemT>> biggerTable_(hashTable_.size() * 2);

        for (const auto& bucket : hashTable_) {
            for (const auto& item : bucket) {
                AddOrEdit();
            }
        }
    }
};

int main() {
    return 0;
}
