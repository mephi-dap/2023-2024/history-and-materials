#include <thread>
#include <iostream>
#include <vector>
#include <random>
#include <chrono>

void PrepareNumbers(std::vector<int>& numbers, size_t amount) {
    std::random_device rd;
    std::mt19937 random(rd());
    numbers.reserve(amount);

    for (size_t i = 0; i < amount; ++i) {
        numbers.push_back(random());
    }
}

void RunInThreads(std::vector<int>& source, size_t threadsAmount, size_t step, std::vector<std::atomic<int>>& dest) {
    auto copyFunc = [&source, &dest, threadsAmount, step] (size_t threadIdx) {
        for (size_t i = threadIdx; i < source.size(); i += threadsAmount) {
            dest[i * step] = source[i];
        }
    };

    const auto timeBefore = std::chrono::steady_clock::now();

    std::vector<std::thread> threads;
    for (size_t i = 0; i < threadsAmount; ++i) {
        threads.emplace_back(copyFunc, i);
    }

    for (auto& thead : threads) {
        thead.join();
    }

    const auto timeAfter = std::chrono::steady_clock::now();
    const auto duration = timeAfter - timeBefore;

    std::cout << "RunInThreads takes " << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() << "ms\n";
}

void RunTest() {
    std::vector<int> source;

    constexpr size_t count = 100000, threadsAmount = 10000, bigStep = 10;

    source.reserve(count);
    PrepareNumbers(source, count);

    std::vector<std::atomic<int>> dest_small(count), dest_big(count * bigStep);

    RunInThreads(source, threadsAmount, 1, dest_small);
    RunInThreads(source, threadsAmount, bigStep, dest_big);
}


int main() {
    for (size_t i = 0; i < 100; ++i) {
        std::cout << "Test #" << i << std::endl;
        RunTest();
        std::cout << std::endl;
    }
}
