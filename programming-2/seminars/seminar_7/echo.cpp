#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <thread>
#include <unistd.h>


void ProcessConnection(int fd) {
    char buffer[1000];
    ssize_t bytesReceived;

    if ((bytesReceived = recv(fd, &buffer, sizeof(buffer), 0)) == -1) {
        perror("recv");
        return;
    }

    if (send(fd, buffer, bytesReceived, 0) == -1) {
        perror("send");
        return;
    }

    if (close(fd) == -1) {
        perror("close");
    }

    write(1, "closed", 6);
}


int main() {
    int socketFd = socket(PF_INET, SOCK_STREAM, 0);
    if (socketFd == -1) {
        perror("Socket");
        return -1;
    }

    struct sockaddr_in address;

    memset(&address, 0, sizeof(struct sockaddr_in));

    address.sin_family = PF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(8080);

    if (bind(socketFd, (sockaddr*)&address, sizeof(sockaddr_in)) == -1) {
        perror("Bind");
        return -1;
    }

    if (listen(socketFd, 5) == -1) {
        perror("Listen");
        return -1;
    }

    while (true) {
        struct sockaddr_in incomingAddress;
        socklen_t addressLen;
        int incomeFd;
        if ((incomeFd = accept(socketFd, (sockaddr*)&incomingAddress, &addressLen)) == -1) {
            perror("Accept");
            return -1;
        }
        std::thread t(ProcessConnection, incomeFd);
        t.detach();
    }

    return 0;
}
