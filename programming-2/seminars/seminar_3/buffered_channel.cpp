#include <iostream>
#include <queue>
#include <thread>

std::mutex coutMutex;

template <typename MessageT>
class BufferedChannel {
   public:
    class ChannelClosed : public std::exception {};

    explicit BufferedChannel(size_t maxMessagesCount) : maxMessagesCount_(maxMessagesCount) {}

    template <typename T>
    void Push(T&& msg) {
        std::unique_lock<std::mutex> lock(mutex_);
        if (closed_) {
            throw ChannelClosed();
        }
        while (messages_.size() == maxMessagesCount_) {
            if (closed_) {
                throw ChannelClosed();
            }
            cv_for_writer_.wait(lock);
        }
        messages_.push(std::forward<T>(msg));
        cv_for_reader_.notify_one();
    }

    MessageT Get() {
        std::unique_lock<std::mutex> lock(mutex_);
        if (closed_) {
            throw ChannelClosed();
        }
        cv_for_reader_.wait(lock, [this]() { return !messages_.empty() || closed_; });
        if (closed_) {
            throw ChannelClosed();
        }
        auto result = messages_.front();
        messages_.pop();
        cv_for_writer_.notify_one();
        return result;
    }

    void Close(bool waitForEmptyQueue = false) {
        std::unique_lock<std::mutex> lock(mutex_);
        if (closed_) {
            throw ChannelClosed();
        }
        if (waitForEmptyQueue) {
            cv_for_writer_.wait(lock, [this]() { return messages_.empty(); });
        }
        closed_ = true;

        cv_for_writer_.notify_all();
        cv_for_reader_.notify_all();
    }

   private:
    const size_t maxMessagesCount_;
    std::queue<MessageT> messages_;
    std::mutex mutex_;
    std::condition_variable cv_for_reader_, cv_for_writer_;
    bool closed_ = false;
};

void Write(BufferedChannel<int>& channel) {
    int i = 0;
    try {
        for (; i < 1000000; ++i) {
            channel.Push(i);
        }
    } catch (const BufferedChannel<int>::ChannelClosed& ex) {
        std::lock_guard<std::mutex> lock(coutMutex);
        std::cout << std::this_thread::get_id() << " (writer): Channel is closed. Written items: " << i << std::endl;
    }
}

void Read(BufferedChannel<int>& channel) {
    int sum = 0;
    size_t counter = 0;

    try {
        while (true) {
            sum += channel.Get();
            ++counter;
        }
    } catch (const BufferedChannel<int>::ChannelClosed& ex) {
        std::lock_guard<std::mutex> lock(coutMutex);
        std::cout << std::this_thread::get_id() << " (reader): Channel is closed. Read items: " << counter << std::endl;
    }

    std::lock_guard<std::mutex> lock(coutMutex);
    std::cout << std::this_thread::get_id() << " sum = " << sum << std::endl;
}

int main() {
    using namespace std::chrono_literals;

    BufferedChannel<int> channel(10);

    std::vector<std::thread> writers, readers;

    for (size_t i = 0; i < 10; ++i) {
        readers.emplace_back(Read, std::ref(channel));
    }

    for (size_t i = 0; i < 100; ++i) {
        writers.emplace_back(Write, std::ref(channel));
    }

    std::this_thread::sleep_for(3s);

    channel.Close();

    for (auto& thread : readers) {
        thread.join();
    }

    for (auto& thread : writers) {
        thread.join();
    }

    return 0;
}
