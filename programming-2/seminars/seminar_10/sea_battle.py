def load_player_map(filename: str) -> list[list[str]]:
    with open(filename) as f:
        m = [
            [
                ch
                for ch in line
                if ch in ".X"
            ]
            for line in f
        ]
        if len(m) < 2 or len(m) > 500:
            raise ValueError(f"File {filename} has too few lines")
        if not all(len(line) == len(m[0]) for line in m):
            raise ValueError(f"File {filename} has lines with different length")
        if len(m[0]) != len(m):
            raise ValueError(f"File {filename} has not square matrix")
        return m


def read_next_turn() -> tuple[int, int] | None:
    try:
        x, y = [int(i) for i in input("Enter x y:\n").split()]
    except Exception as e:
        print(f"Error: {e}")
        raise

    if not (0 < x <= len(player1)):
        print(f"Error: {x} not in range [1, {len(player1)})")
        raise

    if not (0 < y <= len(player1)):
        print(f"Error: {y} not in range [1, {len(player1)})")
        raise

    return x, y


def process_turn(target_player_matrix: list[list[str]], x: int, y: int):
    if target_player_matrix[x][y] == "X":
        target_player_matrix[x][y] = "🔥"
    elif target_player_matrix[x][y] == ".":
        target_player_matrix[x][y] = "💦"
    else:
        raise ValueError(f"Target cell ({x}, {y}) is not X or .")
    print_matrix(target_player_matrix)


def game_is_over():
    player1_lose = not any(ch == "X" for line in player1 for ch in line)
    player2_lose = not any(ch == "X" for line in player2 for ch in line)
    if player1_lose or player2_lose:
        print(f"Player {1 if player1_lose else 2} lose")
    return player1_lose or player2_lose


def print_matrix(matrix: list[list[str]]):
    for row in matrix:
        for ch in row:
            print(ch if ch != "X" else ".", end="")
        print("")


def game_loop():
    player1_turn = True
    while not game_is_over():
        try:
            x, y = read_next_turn()
            process_turn(player2 if player1_turn else player1, x, y)
        except Exception:
            continue

        player1_turn = not player1_turn  # ToDo: real logic for turns order


player1: list[list[str]] | None
player2: list[list[str]] | None


def main():
    global player1, player2
    player1 = load_player_map("/tmp/player1.txt")
    player2 = load_player_map("/tmp/player2.txt")
    if len(player1) != len(player2):
        print("Players should have the same number of rows")
        exit(0)
    game_loop()


if __name__ == "__main__":
    main()
