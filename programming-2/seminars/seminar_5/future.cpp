#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <random>
#include <vector>
#include <variant>
#include <optional>
#include <stdexcept>
#include <exception>
#include <cassert>
#include <iostream>

template<typename T>
class SharedState {
public:
    template<typename V>
    void SetValue(V&& value) {
        std::unique_lock<std::mutex> lock(m_);
        value_ = std::forward<V>(value);
        cv_.notify_all();
    }

    void SetException(const std::exception_ptr& ex) {
        std::unique_lock<std::mutex> lock(m_);
        value_ = ex;
        cv_.notify_all();
    }

    std::optional<T> GetValue() {
        std::unique_lock<std::mutex> lock(m_);
        if (std::holds_alternative<std::exception_ptr>(value_)) {
            std::rethrow_exception(std::get<std::exception_ptr>(value_));
        } else if (std::holds_alternative<T>(value_)) {
            return std::get<T>(value_);
        } else {
            return std::nullopt;
        }
    }

    void Wait() {
        std::unique_lock<std::mutex> lock(m_);
        while (std::holds_alternative<std::monostate>(value_)) {
            cv_.wait(lock);
        }
    }

    bool HasSomething() {
        std::unique_lock<std::mutex> lock(m_);
        return !std::holds_alternative<std::monostate>(value_);
    }

private:
    std::variant<std::monostate, T, std::exception_ptr> value_;
    std::condition_variable cv_;
    std::mutex m_;
};

template<typename T>
class Future {
public:
    explicit Future(std::shared_ptr<SharedState<T>> state) : state_(state) {}

    operator bool() const {
        return state_->HasSomething();
    }

    std::optional<T> GetValue() const {
        return state_->GetValue();
    }

    void Wait() {
        state_->Wait();
    }

    bool IsValid() const {
        return state_;
    }
private:
    std::shared_ptr<SharedState<T>> state_;
};

template<typename T>
class Promise {
public:
    Promise() : state_(std::make_shared<SharedState<T>>()) {}
    Promise(const Promise& other) = delete;
    Promise(Promise&&) = default;

    Promise operator =(const Promise& other) = delete;
    Promise& operator =(Promise&&) = default;

    Future<T> GetFuture() {
        return Future(state_);
    }

    template<typename V>
    void SetValue(V&& value) {
        state_->SetValue(std::forward<V>(value));
    }

    void SetException(const std::exception_ptr& ex) {
        state_->SetException(ex);
    }
private:
    std::shared_ptr<SharedState<T>> state_;
};

void PrepareNumbers(std::vector<int>& numbers, size_t amount) {
    std::random_device rd;
    std::mt19937 random(rd());
    numbers.reserve(amount);

    for (size_t i = 0; i < amount; ++i) {
        numbers.push_back(random());
    }
}

template<typename IterT>
void SubSum(IterT begin, IterT end, Promise<typename IterT::value_type> p) {
    auto result = std::accumulate(begin, end, typename IterT::value_type());
    p.SetValue(std::move(result));
}

template<typename IterT>
Future<typename IterT::value_type> RunSubSum(IterT begin, IterT end) {
    Promise<typename IterT::value_type> p;
    auto f = p.GetFuture();
    std::thread t([begin, end, p = std::move(p)] () mutable {SubSum(begin, end, std::move(p));});
    t.detach();
    return f;
}

int main() {
    std::vector<int> random_numbers;
    std::vector<Future<int>> futures;
    PrepareNumbers(random_numbers, 10000000);

    constexpr int threadsCount = 1000;
    const int itemsPerThread = random_numbers.size() / threadsCount;

//    futures.push_back(RunSubSum(random_numbers.begin(), random_numbers.end()));

    for (auto it = random_numbers.begin(); it < random_numbers.end(); it += itemsPerThread) {
        const auto r = std::min(it + itemsPerThread, random_numbers.end());
        futures.push_back(RunSubSum(it, r));
    }

    const int expectedSum = std::accumulate(random_numbers.begin(), random_numbers.end(), 0);
    int parallelSum = 0;

    for (auto future : futures) {
        future.Wait();
        parallelSum += *future.GetValue();
    }

    assert(expectedSum == parallelSum);

    return 0;
}
