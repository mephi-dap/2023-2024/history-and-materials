# Лекция 14 бонусная. 21 мая
## Тестирование
#### Материалы
[презентация](/programming-2/lectures/Лекция_14.pdf)
# Семинар ---. 18 мая
## Не состоялся к сожалению
# Лекция ---. 14 мая
## Не состоялась к сожалению
# Семинар 13. 11 мая
Контрольная работа: FrozenDict
# Лекция 13. 7 мая
## Subprocess, threading, multiprocessing
#### Материалы
[презентация](/programming-2/lectures/Лекция_13.pdf)
# Семинар 12. 4 мая
#### Материалы
---
# Лекция 12. 30 апреля
## Модули, Пакеты
#### Материалы
[материалы с лекции](/programming-2/lectures/lecture_12.md)
# Семинар 11. 28 апреля
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_11/)
# Лекция 11. 23 апреля
## Исключения
#### Материалы
[материалы с лекции](/programming-2/lectures/lecture_11.md)
# Семинар 10. 20 апреля
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_10/)
# Лекция 10. 16 апреля
## Классы
#### Материалы
[презентация](/programming-2/lectures/Лекция_10.pdf)
# Семинар 9. 13 апреля
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_9/)
# Лекция 9. 9 апреля
## Замыкание, Декораторы, Классы
#### Материалы
[презентация](/programming-2/lectures/Лекция_9.pdf)
# Семинар 8. 6 апреля
Контрольная работа: CheckDir
# Лекция 8. 2 апреля
## Функции, Строки, Файлы/потоки, Пространство имен
#### Материалы
[презентация](/programming-2/lectures/Лекция_8.pdf)
# Семинар 7. 30 марта
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_7/)
# Лекция 7. 26 марта
## Введение в Python
#### Материалы
[презентация](/programming-2/lectures/Лекция_7.pdf)
# Семинар 6. 23 марта
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_6/)
# Лекция ---. 19 марта
## Не состоялась к сожалению
# Семинар 5. 16 марта
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_5/)
# Лекция 6. 12 марта
## Системные вызовы, ввод-вывод
#### Материалы
[материалы с лекции](/programming-2/lectures/lecture_06.md)
# Семинар 4. 9 марта
Контрольная работа: ThreadPool
# Лекция 5. 5 марта
## Promise + Future
#### Материалы
[презентация](/programming-2/lectures/Лекция_5.pdf)
# Семинар 3. 2 марта
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_3/)
# Лекция 4. 27 февраля
## std::atomic, атомарные операции
#### Материалы
[материалы с лекции](/programming-2/lectures/lecture_04.md)
# Семинар 2. 24 февраля
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_2/)
# Лекция 3. 20 февраля
## Condition variable
* Deadlock. Как проверить, что в настоящий момент в hash set одновременно находятся ключи k1, k2, ..., km?
  * Подряд несколько независимых contains допускают гонку: contains(k1) возвращает true, удаляется k1, contains(k2) возвращает true
  * Нужно залочить все ключи, а затем провести проверку
  * Если делать в лоб, то два вызова multicontains(k1, k2) и multicontains(k2, k1) вызовут deadlock
  * Боремся с deadlock через сортировку ключей. Важно сортировать не ключи, а hash-и ключей.
* Semaphore
  * Пропускает ограниченное количество процессов в критическую секцию, остальные зависают
* Condition variable
  * wait
  * notify_one / notify_all
* FixedQueue - очередь ограниченного размера с ожиданием в push/pop при условии заполненности/пустоты на основе std::mutex и std::condition_variable
#### Материалы
  * [презентация](/programming-2/lectures/Лекция_3.pdf)
  * [Код hashset](/programming-2/lectures/lecture_03_code/hashset.h)
  * [Код semaphore](/programming-2/lectures/lecture_03_code/semaphore.h)
  * [Код fixedqueue](/programming-2/lectures/lecture_03_code/fixedqueue.h)
# Семинар 1. 17 февраля
#### Материалы
[материалы с семинара](/programming-2/seminars/seminar_1/)
# Лекция 2. 13 февраля
## Компиляция с++ проектов
#### Материалы
[материалы с лекции](/programming-2/lectures/lecture_02.md)
# Лекция 1. 10 февраля
## Базовая многопоточность: std::thread, std::mutex, std::shared_mutex, std::atomic
#### Материалы
[материалы с лекции](/programming-2/lectures/lecture_01.md)