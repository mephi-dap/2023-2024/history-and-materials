# Лекция №3. 20 февраля
* Deadlock. Как проверить, что в настоящий момент в hash set одновременно находятся ключи k1, k2, ..., km?
  * Подряд несколько независимых contains допускают гонку: contains(k1) возвращает true, удаляется k1, contains(k2) возвращает true
  * Нужно залочить все ключи, а затем провести проверку
  * Если делать в лоб, то два вызова multicontains(k1, k2) и multicontains(k2, k1) вызовут deadlock
  * Боремся с deadlock через сортировку ключей. Важно сортировать не ключи, а hash-и ключей.
* Semaphore
  * Пропускает ограниченное количество процессов в критическую секцию, остальные зависают
* Condition variable
  * wait
  * notify_one / notify_all
* FixedQueue - очередь ограниченного размера с ожиданием в push/pop при условии заполненности/пустоты на основе std::mutex и std::condition_variable
## Материалы
  * [презентация](/programming-2/lectures/Лекция_3.pdf)
  * [Код hashset](/programming-2/lectures/lecture_03_code/hashset.h)
  * [Код semaphore](/programming-2/lectures/lecture_03_code/semaphore.h)
  * [Код fixedqueue](/programming-2/lectures/lecture_03_code/fixedqueue.h)