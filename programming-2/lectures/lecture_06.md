# Системные вызовы, ввод-вывод

### Виртуальная память
Если позволить процессам работать непосредственно с оперативной памятью, то они могут мешать друг другу, записывая свои данные
в одни и те же участки памяти, либо вообще могут затереть данные операционной системы. Также, в случае, когда количество
оперативной памяти недостаточно для размещения всех данных программы, пришлось бы вручную писать код, который часть данных будет
хранить не в оперативной памяти, а на диске. Чтобы упростить работу с памятью и решить эти проблемы, существует "виртуальная память".

Виртуальная память — это концепция, которая позволяет уйти от использования физических адресов, используя вместо них виртуальные, и это даёт ряд преимуществ:
- Расширение реального адресного пространства. Часть виртуальной памяти может быть вытеснена на жёсткий диск, и это позволяет программам использовать больше оперативной памяти, чем есть на самом деле.
- Создание изолированных адресных пространств для различных процессов, что повышает безопасность системы, а также решает проблему привязанности программы к определённым адресам памяти.
- Задание различных свойств для разных участков памяти. Например, может существовать неизменяемый участок памяти, видный нескольким процессам.

При этом вся виртуальная память делится на участки памяти постоянного размера, называемые страницами.
Страница является минимальной единицей выделяемой памяти. Даже если процесс попросит у операционной системы выделить 1 байт памяти,
в действительности выделится не менее чем 1 страница (условно 4096 байт).

Виртуальные адреса преобразуются в физические на аппаратном уровне. (см https://ru.wikipedia.org/wiki/Блок_управления_памятью)

Пользовательское пространство (user space) — адресное пространство виртуальной памяти операционной системы, отводимое для пользовательских программ,
в отличие от пространства ядра (kernel space), которое резервируется для работы ядра операционной системы, его расширений и, возможно, некоторых драйверов устройств.

### Time sharing
Один процессор (одно ядро) может в один момент времени заниматься исполнением одной программы. Чтобы запускать на одном процессоре
много программ сразу от имени одного пользователя или нескольких, процессорное время делится между всеми программами.
Операционная система отвечает за планирование процессорного времени. Более приоритетные процессы получают больше
времени, менее приоритетные меньше. Во время операций ввода-вывода выполнение процесса прерывается и время отдается другим.

### Что такое системные вызовы
За взаимодействие с оборудованием (монитор, жесткий диск, оперативная память, сеть, клавиатура и т.д.) отвечает операционная система.
Процессы, запускаемые пользователями, не могут просто так обращаться к нему, а также не могут получать доступ к данным других процессов.
Для выполнения межпроцессной операции или операции, требующей доступа к оборудованию, программа обращается к ядру, которое, в зависимости от полномочий вызывающего процесса, исполняет либо отказывает в исполнении такого вызова.
Системный вызов — способ обращения программы пользовательского пространства к пространству ядра. В момент системного вызова
выполнение кода программы прерывается и начинает выполняться код операционной системы.

При написании программ мы, как правило, не совершаем системные вызовы вручную, а пользуемся библиотеками, совершающими системные вызовы за нас.

Как именно реализованы системные вызовы, можно почитать тут https://habr.com/ru/articles/347596/

### Как посмотреть, какие системные вызовы совершает программа
В Linux с помощью утилиты `strace` можно посмотреть, какие системные вызовы совершает процесс.
В Mac OS и других UNIX-like есть аналогичные утилиты `dtruss`, `ktrace`, `dtrace`.

Кто запускает на Mac OS, может возникнуть проблема с тем, что операционная система не разрешит логировать
системные вызовы из-за включенной System Integrity Protection.
System Integrity Protection можно отключить при необходимости, но лучше потом обратно включить.

В иных операционных системах тоже есть свои аналоги (см https://en.wikipedia.org/wiki/Strace#Similar_tools).

### Hello, World!
Рассмотрим такую программу
```c++
#include <iostream>

int main() {
    std::cout << "Hello, World!\n";
    return 0;
}
```

Скомпилируем и запустим её под `strace` на Linux
```shell
g++ -o simple_hello_world simple_hello_world.cpp
strace ./simple_hello_world 2> /tmp/trace.txt  # перенаправили вывод strace в файл /tmp/trace.txt
```

В файле `/tmp/trace.txt` можно будет увидеть много строк, каждая из которых соответствует какому-то системному
вызову, сделанному программой `simple_hello_world`. Например
```text
execve("./simple_hello_world", ["./simple_hello_world"], 0x7ffc4b6c8d40 /* 27 vars */) = 0
brk(NULL)                               = 0x5556c4005000
arch_prctl(0x3001 /* ARCH_??? */, 0x7ffff10350d0) = -1 EINVAL (Invalid argument)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=56468, ...}) = 0
mmap(NULL, 56468, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f61565bd000
close(3)                                = 0
openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libstdc++.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0`\341\t\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0644, st_size=1956992, ...}) = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f61565bb000

... тут еще много вызовов

munmap(0x7f61565bd000, 56468)           = 0
brk(NULL)                               = 0x5556c4005000
brk(0x5556c4026000)                     = 0x5556c4026000
fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x2), ...}) = 0
write(1, "Hello, World!\n", 14)         = 14
exit_group(0)                           = ?
+++ exited with 0 +++
```

В каждой строке сначала указано имя системного вызова, затем в скобках указываются аргументы, затем после знака `=` результат.
Если вызов вернул -1, что как правило означает ошибку, далее будет идти значение переменной `errno`.
Чтобы узнать, что делает тот или иной системный вызов, на UNIX-like системах можно воспользоваться утилитой `man`.
Например, посмотрите `man execve`, `man access` или `man brk`.

В данном случае мы видим, что прежде чем вывести в стандартный поток вывода фразу `"Hello, World!"`, процесс сделал
много системных вызовов, чтобы загрузить динамические библиотеки.

## Файловый ввод-вывод
Файловый дескриптор – виртуальный объект, может представлять как файл на диске, так и pipe, сокет или устройство.

Например, чтение из `/dev/random`. С точки зрения программы это просто файл, из которого можно что-то прочитать.
На самом деле это не какой-то файл, лежащий на диске и содержащий конкретные данные, а виртуальное устройство,
генерирующее рандомные байты (есть еще похожий файл `/dev/urandom`, но там могут быть псевдослучайные байты).

По сути это число (id, идентификатор), соответствующее структуре с информацией об открытом файле в ядре.
При открытии файла операционная система выдает процессу дескриптор для дальнейшей работы с этим файлом.
Выше в примере с `simple_hello_world` была строка `openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3`.
Тут написано, что наш процесс попросил операционную систему открыть файл `/etc/ld.so.cache` для чтения, а операционная система
в ответ выдала ему дескриптор `3`, с помощью которого процесс сможет дальше совершать другие операции с этим файлом.
Для разных процессов одинаковые по значению дескрипторы могут значить разное. Например, в других процессах под номером
`3` могут быть обозначены другие файлы.

При запуске процесса ему изначально выдаются три файловых дескриптора, которые не нужно открывать отдельными системными
вызовами в ходе выполнения программы.
- 0 – stdin, стандартный поток ввода. При запуске программы из терминала он получает ввод с клавиатуры
- 1 – stdout, стандартный поток вывода. При запуске программы из терминала он выводит данные в терминал
- 2 – stderr, стандартный поток вывода ошибок. При запуске программы из терминала он выводит данные в терминал

В примере с `simple_hello_world` мы выводили строку `"Hello, World!\n"` в "файл" с дескриптором `1`, что соответствует потоку stdout.

Можем вручную вызвать функцию `write` из библиотеки unix, которая "под капотом" сделает системный вызов `write`

```c
#include <unistd.h>

int main() {
    // 1 – дескриптор стандартного потока вывода stdout
    // "Hello!\n" – массив байт для вывода в поток
    // 7 – сколько байт из массива надо вывести в поток
    write(1, "Hello!\n", 7);
    return 0;
}
```

Собираем и запускаем
```shell
gcc -o simple_hello_world simple_hello_world.c
strace ./simple_hello_world 2> /tmp/trace.txt
```

На этот раз вывод `strace` гораздо меньше.
```text
execve("./simple_hello_world", ["./simple_hello_world"], 0x7fffdd04dd70 /* 27 vars */) = 0
brk(NULL)                               = 0x5562da0eb000
arch_prctl(0x3001 /* ARCH_??? */, 0x7ffd4e608c30) = -1 EINVAL (Invalid argument)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=56468, ...}) = 0
mmap(NULL, 56468, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7fd75cc6c000
close(3)                                = 0
openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\300A\2\0\0\0\0\0"..., 832) = 832
pread64(3, "\6\0\0\0\4\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0"..., 784, 64) = 784
pread64(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32, 848) = 32
pread64(3, "\4\0\0\0\24\0\0\0\3\0\0\0GNU\0\30x\346\264ur\f|Q\226\236i\253-'o"..., 68, 880) = 68
fstat(3, {st_mode=S_IFREG|0755, st_size=2029592, ...}) = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7fd75cc6a000
pread64(3, "\6\0\0\0\4\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0@\0\0\0\0\0\0\0"..., 784, 64) = 784
pread64(3, "\4\0\0\0\20\0\0\0\5\0\0\0GNU\0\2\0\0\300\4\0\0\0\3\0\0\0\0\0\0\0", 32, 848) = 32
pread64(3, "\4\0\0\0\24\0\0\0\3\0\0\0GNU\0\30x\346\264ur\f|Q\226\236i\253-'o"..., 68, 880) = 68
mmap(NULL, 2037344, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7fd75ca78000
mmap(0x7fd75ca9a000, 1540096, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x22000) = 0x7fd75ca9a000
mmap(0x7fd75cc12000, 319488, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x19a000) = 0x7fd75cc12000
mmap(0x7fd75cc60000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1e7000) = 0x7fd75cc60000
mmap(0x7fd75cc66000, 13920, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7fd75cc66000
close(3)                                = 0
arch_prctl(ARCH_SET_FS, 0x7fd75cc6b540) = 0
mprotect(0x7fd75cc60000, 16384, PROT_READ) = 0
mprotect(0x5562d9ac6000, 4096, PROT_READ) = 0
mprotect(0x7fd75cca7000, 4096, PROT_READ) = 0
munmap(0x7fd75cc6c000, 56468)           = 0
write(1, "Hello!\n", 7)                 = 7
exit_group(0)                           = ?
+++ exited with 0 +++
```

Можем написать похожую программу вот так
```c
#include <stdio.h>

int main() {
    fwrite("Hello!\n", 1, 7, stdout);
    return 0;
}
```

Разница в том, что ранее мы использовали функцию `write` из системной библиотеки `unistd.h`, которая делала для нас непосредственно
одноименный системный вызов `write` и передавала в него наши аргументы. Теперь же мы используем функцию `fwrite` из
стандартной библиотеки языка C, которая, хоть и тоже в итоге приводит в системному вызову `write`, но делает это не сразу.
Функция `fwrite` использует дополнительный внутренний буфер, накапливая в нем данные до тех пор, пока он не заполнится, и лишь потом делает `write`.

При использовании библиотеки `iostream` в C++ кроме дополнительной буферизации ввода-вывода также происходит
применение локали (байты интерпретируются как символы в какой-то кодировке), разбиение строк на токены при чтении, форматирование при выводе.

На самом деле при использовании `write` тоже есть буферизация, только она происходит уже внутри системного вызова на уровне ядра операционной системы.
Чтобы попросить операционную систему очистить буфер и завершить запись данных в файл, можно использовать системный вызов `fsync` или `fdatasync`.
