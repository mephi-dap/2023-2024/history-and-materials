# Исключения
- Бросить исключение – ключевое слово `raise`. Аналог `throw` в C++
- Ловить исключения – ключевое слово `except`. Аналог `catch` в C++
- Выполнить какие-то действия гарантировано, вне зависимости от того, выброшено исключение или нет – `finally`
- Выполнить какие-то действия только если исключения не произошло – `else`
- Базовый класс для всех "обычных" исключений – `Exception`
- Если выполнение программы прерывают с помощью SIGINT (условно, нажали на клавиатуре Ctrl-C в терминале),
то выбрасывается исключение `KeyboardInterrupt`, которое **не** наследуется от `Exception`, чтобы его было
легко обработать отдельно и случайно не перехватить там, где не надо

```python
class SuperError(Exception):
    pass


def raises(val):
    if val is None:
        raise SuperError("Can't be None")
    return int(val)


def main():
    try:
        raises(5)
    except Exception:
        pass
    else:
        print("Nothing was raised")
    finally:
        print("First example finished")

    try:
        raises(None)
    except SuperError as e:
        print(e)
    except Exception as e:
        print(f"Something went wrong: {e}")
    else:
        print("Nothing was raised")
    finally:
        print("Second example finished")

    try:
        raises("lol")
    except (ValueError, TypeError, SuperError) as e:
        print(f"Something went wrong: ({type(e).__name__}) {e}")
        raise  # пробросим текущее обрабатываемое исключение выше по стеку вызовов, как будто его никто не ловил тут


if __name__ == "__main__":
    main()
```

Пример обработки `KeyboardInterrupt`
```python
import time


def wait_long_time():
    print("Waiting long time...")
    time.sleep(10)
    raise ValueError("You waited too long 😈")


def main():
    try:
        wait_long_time()
    except Exception as e:
        print(f"Exception: {e}")
    except KeyboardInterrupt:
        print("Keyboard Interrupt")
    finally:
        print("Good bye!")


if __name__ == "__main__":
    main()
```

# Контекстные менеджеры
- Вместо RAII в Python используются контекстные менеджеры
- Способ контролировать освобождение ресурсов/откат изменений в блоке кода
- Типичный пример контекстного менеджера – работа с открытыми файлами
  - После открытия файла заходим в контекстный менеджер с помощью ключевого слова `with`
  ```python
  with open("file.txt") as f:
      pass
  ```
  - При выходе из блока кода автоматически вызывается метод `close` у `f`
  - Даже если в блоке кода произойдет исключение или `return`, контекстный менеджер выполнит необходимое действие при
  выходе из блока
- Можно зайти в несколько контекстных менеджеров в одном блоке
```python
c = open("c.txt")
with foo() as a, Bar() as b, c:
    pass
```

Как сделать свой контекстный менеджер. Рассмотрим на примере
```python
class BreadcrumbsHolder:
    def __init__(self):
        self.breadcrumbs: list[str] = []

    def push(self, item: str):
        self.breadcrumbs.append(item)

    def pop(self):
        self.breadcrumbs.pop()

    def get_breadcrumbs(self):
        return s + ": " if (s := " - ".join(self.breadcrumbs)) else ""


class BreadcrumbContextManager:
    def __init__(self, bh: BreadcrumbsHolder, item: str):
        self.bh = bh
        self.item = item

    def __enter__(self):
        """
        Вызывается при входе в блок кода под "with"
        """
        self.bh.push(self.item)

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Вызывается при выходе из блока кода.
        exc_type – тип исключения, которое возникло в блоке кода
        exc_val – само исключение
        exc_tb – стек вызовов в момент возникновения исключения.
        Если исключения не было, то везде будет None
        """
        if exc_type is not None:
            print(f"{self.bh.get_breadcrumbs()}Oh, no! {exc_type.__name__}: {exc_val}")
        self.bh.pop()


def breadcrumb(bh: BreadcrumbsHolder, item: str):
    return BreadcrumbContextManager(bh, item)


def log(bh: BreadcrumbsHolder, message: str):
    print(bh.get_breadcrumbs() + message)


def main():
    bh = BreadcrumbsHolder()
    log(bh, "First message")

    try:
        with breadcrumb(bh, "Level 1"):
            with breadcrumb(bh, "Level 2"), breadcrumb(bh, "Level 3"):
                log(bh, "Second message")
            log(bh, "Third message")
            raise ValueError("Lol")
            log(bh, "Unreachable message")
    except Exception as e:
        log(bh, f"Exception occurred: {e}")

    log(bh, "Fourth message")


if __name__ == "__main__":
    main()
```

Пример с таймером
```python
import time


class Timer:
    def __enter__(self):
        self.t = time.time_ns()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.e = time.time_ns()

    def milliseconds(self):
        return float(self.e - self.t) / 10**6


def main():
    with Timer() as t1:
        s = sum(x * x for x in range(200000))
        print(f"Sum = {s}")
    print(f"Time taken = {t1.milliseconds():.3f}")


if __name__ == "__main__":
    main()
```

# Итераторы

- Если мы хотим, чтобы по какому-то объекту можно было итерироваться, то у него должен быть объявлен метод `__init__`, который возвращает итератор
- Метод `__init__` вызывается, когда в коде инициируется итерация. Например, с помощью цикла `for elem in obj` или явным вызовом `iter(obj)`

- Итератор в свою очередь это тоже объект, по которому можно итерироваться, например, с помощью цикла `for`
- Итератор должен предоставлять методы `__iter__` и `__next__`
- Метод `__iter__` должен возвращать итератор (можно вернуть самого себя, либо какой-то новый итератор)
- Метод `__next__` вызывается перед каждой итерацией. Должен инкрементировать итератор и возвращать какое-то полезное значение
  - Чтобы обозначить конец итераций, метод `__next__` должен выбросить исключение `StopIteration`

Пример, итерация по числам Фибоначчи
```python
import copy


class FibonacciIter:
    def __init__(self, numbers_count: int, start_from_index: int = 1):
        self.a = 1
        self.b = 1
        self.numbers_count = numbers_count

        while start_from_index > 1:
            start_from_index -= 1
            self._inc()

    def _inc(self):
        c = self.a + self.b
        self.a = self.b
        self.b = c

    def __iter__(self):
        """
        В данном случае метод возвращает тот же самый итератор.
        """
        return self

    def __next__(self):
        """
        Переходим к следующему числу Фибоначчи. Возвращаем текущее
        """
        if self.numbers_count == 0:
            raise StopIteration
        self.numbers_count -= 1
        self._inc()
        return self.a


def main():
    print("First 10 Fibonacci numbers:")
    for fib in FibonacciIter(10):
        print(fib)

    print("Fibonacci numbers from 10th to 25th:")
    for fib in FibonacciIter(15, 10):
        print(fib)

    print("Fibonacci numbers with enumeration:")
    for i, fib in enumerate(FibonacciIter(10)):
        print(f"Fibonacci number №{i+1} is {fib}")

    iterator = FibonacciIter(10)
    iterator2 = copy.copy(iterator)
    for _ in iterator:
        pass
    # Дошли до конца в итераторе "iterator"

    for fib in iterator:
        # Ни разу не попадем в тело цикла, так как итератор "iterator" уже дошел до конца,
        # и повторный вызов метода __next__ опять выбросит исключение
        print(f"Nothing happens: {fib}")
        assert False

    for fib in iterator2:
        print(f"Something happens: {fib}")
        break

    print("Manual iterations:")
    try:
        it = iter(FibonacciIter(10))
        while val := next(it):
            print(val)
    except StopIteration:
        print("StopIteration")


if __name__ == "__main__":
    main()
```

Теперь рассмотрим пример, в котором будем итерироваться по контейнеру со строками и склеивать их
```python
class StringConcatenator:
    class Iterator:
        def __init__(self, concatenator):
            self.concatenator = concatenator
            self.string_index = 0
            self.string_position = 0

        def __iter__(self):
            return self

        def __next__(self):
            if self.string_index == len(self.concatenator.strings):
                raise StopIteration
            current_character = self.concatenator.strings[self.string_index][self.string_position]
            self.string_position += 1
            if self.string_position == len(self.concatenator.strings[self.string_index]):
                self.string_position = 0
                self.string_index += 1
            return current_character

    def __init__(self):
        self.strings: list[str] = []

    def append(self, string: str):
        self.strings.append(string)

    def __len__(self):
        return sum(len(s) for s in self.strings)  # <-- generator expression, см дальше в лекции

    def __iter__(self):
        """
        Создали новый итератор, который будет пробегать все символы с начала первой строки и далее
        """
        return self.Iterator(self)

    def __str__(self):
        return "".join(self)  # join внутри делает итерации по тому контейнеру, который ему дали


def main():
    sc = StringConcatenator()
    sc.append("Hello, ")
    sc.append("World!")
    sc.append(" Some string")

    print("Enumerate all characters:")
    for i, c in enumerate(sc):
        print(f"({i+1:02}/{len(sc)}): {c}")

    print("Here we go again:")
    for c in sc:
        print(c, end="")

    print("\nWrap into list:")
    print(list(sc))

    print("Unique characters:")
    print(set(sc))

    print("Unique characters ignoring case and not alphabetic characters:")
    print(set(c.lower() for c in sc if c.isalpha()))  # <-- generator expression, см дальше в лекции

    print(f"str(sc): {str(sc)}")


if __name__ == "__main__":
    main()
```

# Генераторы
- Функция, возвращающая итератор. Может делать это не явно через `return`, а с помощью `yield`
- Инструкция `yield <value>` прерывает выполнение функции, возвращает значение, и запоминает текущий контекст.
- Выполнение функции может быть продолжено "с того же места", с сохранением значения всех локальных переменных
- Генераторы позволяют пробежать по последовательности элементов, которые генерируются "на лету"
- Часто вместо описания итератора в виде класса удобно сделать функцию-генератор

Предыдущий пример, переписанный через генератор вместо класса-итератора
```python
class StringConcatenator:
    def __init__(self):
        self.strings: list[str] = []

    def append(self, string: str):
        self.strings.append(string)

    def __len__(self):
        return sum(len(s) for s in self.strings)

    def __iter__(self):  # <-- генератор
        for s in self.strings:
            for c in s:
                yield c

    def __str__(self):
        return "".join(self)
```

Числа Фибоначчи, только теперь с помощью генератора
```python
def fibonacci(numbers_count: int, start_from_index: int = 1):
    a = 1
    b = 1

    while start_from_index > 1:
        start_from_index -= 1
        a, b = b, a + b

    while numbers_count > 0:
        yield b
        numbers_count -= 1
        a, b = b, a + b
```

# Generator expression – краткая форма генераторов.
- Синтаксически похоже на list comprehension, но вместо квадратных скобок круглые
- Если generator expression используется в качестве единственного аргумента функции, то скобки можно не писать

То же самое можно написать проще через generator expressions.
```python
class StringConcatenator:
    def __init__(self):
        self.strings: list[str] = []

    def append(self, string: str):
        self.strings.append(string)

    def __len__(self):
        return sum(len(s) for s in self.strings)  # тут скобки можно не писать, так как единственный аргумент 

    def __iter__(self):
        return (c for s in self.strings for c in s)  # <-- generator expression

    def __str__(self):
        return "".join(self)
```

### Лайфхак
С помощью библиотеки contextlib можно писать контекстные менеджеры кратко через генераторы
```python
import contextlib
import time


class Timer:
    def __init__(self):
        self.t = time.time_ns()
        self.e = self.t

    def stop(self):
        self.e = time.time_ns()

    def milliseconds(self):
        return float(self.e - self.t) / 10**6


@contextlib.contextmanager
def timer():
    t = Timer()
    try:
        yield t
    finally:
        t.stop()


def main():
    with timer() as t1:
        s = sum(x * x for x in range(200000))
        print(f"Sum = {s}")
    print(f"Time taken = {t1.milliseconds():.3f}")


if __name__ == "__main__":
    main()
```

