#include <iostream>
#include <mutex>
#include <shared_mutex>
#include <vector>

struct TNode
{
    int Key;
};

struct TBucket
{
    mutable std::mutex Mutex;
    std::vector<TNode> Nodes;
};

class THashSet
{
public:
    THashSet()
        : Buckets_(42)
    { }

    bool Multicontains(int k1, int k2) const {
        std::shared_lock rehashGuard(RehashMutex_);
        std::lock_guard guard1(Buckets_[CalculateBucketIndex(k1)].Mutex);
        std::lock_guard guard2(Buckets_[CalculateBucketIndex(k2)].Mutex);
        return ContainsImpl(k1) && ContainsImpl(k2);
    }

    bool Contains(int key) const
    {
        std::shared_lock rehashGuard(RehashMutex_);
        std::lock_guard guard(Buckets_[CalculateBucketIndex(key)].Mutex);
        return ContainsImpl(key);
    }

    bool Insert(int key)
    {
        std::shared_lock rehashGuard(RehashMutex_);
        auto bucketIndex = CalculateBucketIndex(key);
        std::lock_guard guard(Buckets_[bucketIndex].Mutex);
        if (ContainsImpl(key)) {
            return false;
        }
        Buckets_[bucketIndex].Nodes.push_back(TNode{key});
        // TODO: Invoke rehashing.
        return true;
    }

    bool Remove(int key)
    {
        std::shared_lock rehashGuard(RehashMutex_);
        auto bucketIndex = CalculateBucketIndex(key);
        std::lock_guard guard(Buckets_[bucketIndex].Mutex);
        auto& bucket = Buckets_[bucketIndex].Nodes;
        auto it = std::remove_if(
            bucket.begin(),
            bucket.end(),
            [key] (const auto& node) {
                return node.Key == key;
            });
        if (it == bucket.end()) {
            return false;
        } else {
            bucket.erase(it, bucket.end());
            return true;
        }
    }

private:
    const std::hash<int> Hasher_;
    mutable std::shared_mutex RehashMutex_;
    std::vector<TBucket> Buckets_;

private:
    int CalculateBucketIndex(int key) const
    {
        return Hasher_(key) % Buckets_.size();
    }
    bool ContainsImpl(int key) const
    {
        auto bucketIndex = CalculateBucketIndex(key);
        for (const auto& node : Buckets_[bucketIndex].Nodes) {
            if (node.Key == key) {
                return true;
            }
        }
        return false;
    }
    void Rehash()
    {
        std::unique_lock guard(RehashMutex_);
        // TODO: Rehashing.
    }
};
