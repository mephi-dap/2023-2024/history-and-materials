#include <mutex>
#include <queue>

class FixedQueue {
public:
    explicit FixedQueue(int n)
        : max_size_(n)
    { }

    void Push(int x) {
        std::unique_lock lock(mtx);
        while (queue_.size() == max_size_) {
            full.wait(lock);
        }
        queue_.push(x);
        empty.notify_one();
    }

    int Pop() {
        std::unique_lock lock(mtx);
        while (queue_.empty()) {
            empty.wait(lock);
        }
        const int result = queue_.front();
        queue_.pop();
        full.notify_one();
        return result;
    }

private:
    std::mutex mtx;
    std::condition_variable full;
    std::condition_variable empty;
    std::queue<int> queue_;
    int max_size_;
};
