#include <mutex>
#include <sleep>

 struct condition_variable {
     void wait(std::unique_lock&);
     void notify_one();
     void notify_all();
 };


class Semaphore {
public:
    Semaphore(int n) : count_(n) { }

    void Enter();
    void Leave();

private:
    int count_ = 0;
};

class SemaphoreBad1 {
public:
    SemaphoreBad1(int n)
        : count_(n)
    { }

    void Leave() {
        std::unique_lock guard(mutex_);
        ++count_;
    }

    void Enter() {
        std::unique_lock guard(mutex_);
        while (count_ == 0) { // <-- Busy wait
            guard.unlock();
            sleep(...);
            guard.lock();
        }
        --count_;
    }
private:
    std::mutex mutex_;
    int count_;
};

class SemaphoreBad2 {
public:
    SemaphoreBad2(int n)
        : count_(n)
    { }

    void Leave() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) cv_.notify_one();
        ++count_;
    }

    void Enter() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) {
            cv_.wait(); // <-- Deadlock
        }
        --count_;
    }
private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

class SemaphoreBad3 {
public:
    SemaphoreBad3(int n)
        : count_(n)
    { }

    void Leave() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) cv_.notify_one();
        ++count_;
    }

    void Enter() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) {
            guard.unlock();
            cv_.wait(); // <-- Lost wakeup
            guard.lock();
        }
        --count_;
    }
private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

class SemaphoreBad4 {
public:
    SemaphoreBad4(int n)
        : count_(n)
    { }

    void Leave() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) cv_.notify_one();
        ++count_;
    }

    void Enter() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) {
            cv_.wait(guard);
        } // <-- count_ == 0
        --count_;
    }
private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

class SemaphoreBad5 {
public:
    SemaphoreBad5(int n)
        : count_(n)
    { }

    void Leave() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) cv_.notify_one(); // <-- Lost notify
        ++count_;
    }

    void Enter() {
        std::unique_lock guard(mutex_);
        cv_.wait(guard, [this] { return count_ > 0; });
        --count_;
    }
private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

class SemaphoreGood {
public:
    SemaphoreGood(int n)
        : count_(n)
    { }

    void Leave() {
        std::unique_lock guard(mutex_);
        cv_.notify_one();
        ++count_;
    }

    void Enter() {
        std::unique_lock guard(mutex_);
        cv_.wait(guard, [this] { return count_ > 0; });
        --count_;
    }
private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};



