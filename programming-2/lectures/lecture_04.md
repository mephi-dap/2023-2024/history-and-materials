# std::atomic, атомарные операции

Атомарная операция является неделимой, она либо полностью произошла, либо нет.
Невозможно увидеть промежуточное состояние атомарных операций или какие-либо частичные эффекты их применения.

Типы `std::atomic<T>` являются оберткой над примитивными или тривиально-копируемыми типами данных,
позволяющими удобно применять атомарные операции вместо обычных +, -, *, /, =, ++ и т.д.

Атомарные операции разделяются на операции чтения (load), записи (store) и чтения-модификации-записи (read-modify-write, RMW).

Кроме того, можно вручную вызывать атомарные операции, рассмотрим некоторые из них:

- load – атомарно получить значение из переменной
  ```c++
  std::atomic<int> someValue;
  // ... что-то делали с someValue
  int valueCopy = someValue.load();
  // ...
  ```
  - Нельзя просто присвоить через оператор `=` значение из одной `std::atomic` переменной в другую, так как это подразумевает две
отдельные операции – загрузить значение из одной переменной в процессор и затем записать это значение в другую переменную.
Для этого нужно использовать `load`, который явно вызовет атомарную операцию чтения, а затем использовать `=`, которое вызовет
явное атомарное присваивание этого значения в другую переменную

- store – атомарно записать значение в переменную
  ```c++
  std::atomic<int> variable;
  // ...
  variable.store(150);
  ```
  - Зачем нужен store, если есть `=`. В функциях load/store есть аргумент, который задает упорядочивание операций, работающих
с памятью. По умолчанию используется самый строгий порядок (о барьерах памяти позже)

- compare and swap (CAS) – сравнить текущее значение переменной с данным ожидаемым значением,
и заменить текущее на новое переданное значение, если оно совпадает с ожидаемым,
а на место нового значения записать старое значение переменной (меняет местами старое и новое значение, делает swap).
Возвращает `true`, если обмен состоялся. Если обмен не состоялся, то возвращает `false` и записывает текущее значение переменной вместо ожидаемого
  - Операция типа RMW
  - В c++ реализовано через методы `compare_exchange_weak` и `compare_exchange_strong`
  - `compare_exchange_weak` – может вернуть `false` даже если текущее значение переменной равно ожидаемому.
На низком уровне реализовано через две отдельные атомарные операции, работающие с памятью. Операционная система может прервать
поток между ними и из-за этого вся сложная операция CAS не выполнится, хотя в переменной могло быть ожидаемое значение
  - `compare_exchange_strong` – всегда возвращает `true` и выполняет обмен значений, если текущее значение равно ожидаемому.
На низком уровне реализовано в виде единой тяжелой операции CAS
  - Более подробно их описание и сравнение можно посмотреть тут https://en.cppreference.com/w/cpp/atomic/atomic/compare_exchange
  - Рассмотрим на примере `compare_exchange_weak`
  ```c++
  template<typename T>
  struct Node {
      T data;
      Node* next;
      Node(const T& data) : data(data), next(nullptr) {}
  };
   
  template<typename T>
  class Stack {
  private:
      std::atomic<Node<T>*> head;
  public:
      void push(const T& data) {
          Node<T>* new_node = new Node<T>(data);
   
          new_node->next = head.load();  // атомарная операция чтения
   
          // если значение в поле head все еще равно new_node->next, то заменим его на new_node и выйдем из цикла
          // если оно уже поменялось, то compare exchange положит текущее значение переменной head в new_node->next и вернет false
          while (
              !head.compare_exchange_weak(
                  new_node->next,  // ожидаемое значение
                  new_node  // новое значение
              )
          );  // тело цикла пустое, все интересное в условии
      }
  };
  ```
- exchange – атомарно записывает данное значение в переменную и возвращает предыдущее значение, которое было в переменной
  - Операция типа RMW
  - С помощью такой операции можно реализовать аналог мьютекса, который называется spinlock
  ```c++
  class TASSpinLock {
  public:
    void Lock() {
      while (flag_.exchange(true)) {
        // sleep / pause
      }
    }

    void Unlock() {
        flag_.store(false);
    }

  private:
    std::atomic<bool> flag_{false};
  };
  ```
  
- test and set (TAS) – устанавливает значение атомарного флага в `true` и возвращает старое значение.
  - Операция типа RMW
  - Применимо к `std::atomic_flag`. Это более специфический аналог `std::atomic<bool>`
  - Можно переделать предыдущий пример так
  ```c++
  class TASSpinLock {
  public:
    void Lock() {
      while (flag_.test_and_set()) {
        // sleep / pause
      }
    }

    void Unlock() {
        flag_.clear();  // сброс флага в false
    }

  private:
    std::atomic_flag flag_;
  };
  ```
- wait – если текущее значение переменной равно данному, то подождать (остановить текущий поток) до тех пор пока поток не будет
разбужен
- notify_all/notify_one – разбудить поток (все или один соответственно), который ранее вызывал wait с атомарной переменной 
- fetch_add – атомарно заменяет текущее значение переменной на сумму текущего значения и данного аргумента.
  - Есть аналог с вычитанием `fetch_sub`
  - Рассмотрим пример реализации мьютекса с помощью `fetch_add`, `wait` и `notify_one`
  ```c++
  class Mutex {
  public:
    void Lock() {
        count_.fetch_add(1);
        while (state_.exchange(1) == 1) {
            state_.wait(1);
        }
    }
  
    void Unlock() {
        state_.store(0);
        if (count_.fetch_sub(1) > 1) {
            state_.notify_one();
        }
    }
  
  private:
    std::atomic<int> state_{0};
    std::atomic<int> count_{0};
  };
  ```

### Упорядочивание операций работы с памятью
- Компилятор и CPU могут переупорядочивать операции чтения/записи (при условии, что это переупорядочивание никак не затронет функциональность однопоточного кода), то есть могут быть фактически выполнены в порядке, отличном от того, который определен в коде. Проблема с этим может возникнуть в многопоточной среде, когда потоки, запускаемые на разных CPU, при общении могут увидеть операции в таком переупорядоченом порядке, который ломает логику работы с общими данными.
- Обычно такие проблемы решаются с помощью мьютексов, но мы обсуждаем атомики. И в этом контексте memory ordering — это enum, определяющий, какие типы переупорядочиваний запрещены:
  - `memory_order_acquire`: гарантирует, что ни одна операция чтения не будет перемещена до текущей операции чтения.
  - `memory_order_release`: гарантирует, что ни одна операция записи не будет перемещена после текущей операции.
  - `memory_order_acq_rel`: комбинация двух предыдущих гарантий.
  - `memory_order_relaxed`: вообще не дает никаких гарантий по переупорядочиванию.
  - `memory_order_seq_cst`: cамый строгий memory ording, дефолтный для операций чтения/записи, запрещающий любые переупорядочивания.
- Подробнее можно почитать тут https://en.cppreference.com/w/cpp/atomic/memory_order и тут https://habr.com/ru/articles/197520/


### Гарантия однократного вызова с помощью std::once_flag, паттерн Singleton
- Singleton – объект такого типа может существовать в единственном экземпляре
- Можно реализовать разными способами. Возникает вопрос, как гарантировать единственность экземпляра, когда программа многопоточная.
Как случайно не создать несколько объектов одного типа?

- Вариант с `std::once_flag` и `std::call_one`. Внутри происходит атомарная проверка флага
  ```c++
  #include <atomic>
  #include <iostream>
  #include <memory>
  #include <mutex>
  #include <thread>
  #include <vector>
  
  using namespace std::chrono_literals;
  
  class Config {
  public:
      Config() {
          std::this_thread::sleep_for(500ms);
          std::cout << "Config initialized\n";
      }
  
      bool GetFlag() const {
          return true;
      }
  };
  
  template <typename T>
  class Singleton {
  private:
      static inline std::once_flag flag_;
      static inline std::unique_ptr<T> instance_;
  
  public:
      static T& Get() {
          std::call_once(flag_, Singleton::Init);
          return *instance_;
      }
  
      static void Init() {
          instance_ = std::make_unique<T>();
      }
  };
  
  using GlobalConfig = Singleton<Config>;
  
  void UseConfig() {
      std::cout << "Before GetFlag()\n";
      GlobalConfig::Get().GetFlag();
      std::cout << "After GetFlag()\n";
  }
  
  int main() {
      std::vector<std::thread> threads;
      for (size_t i = 0; i < 100; ++i) {
          threads.emplace_back(UseConfig);
      }
  
      for (auto& thread : threads) {
          thread.join();
      }
  
      return 0;
  }
  ```
  
- Singleton Мейерса, также гарантируется единственный экземпляр, но без явной проверки. Компилятор гарантирует однократную инициализацию static переменной.
  ```c++
  template <typename T>
  class MeyersSingleton {
  public:
    static T& Get() {
        static T instance;
        return instance;
    }
  };
  ```
