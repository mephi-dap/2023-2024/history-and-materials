from .lorem_ipsum import get_lorem_ipsum
from .submodule import foo


def print_hi(s):
    print(f"Hi, {s}!")


__all__ = ["print_hi", "get_lorem_ipsum"]