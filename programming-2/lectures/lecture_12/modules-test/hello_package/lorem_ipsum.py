import requests


def get_lorem_ipsum(paragraphs: int):
    response = requests.get(
        "https://baconipsum.com/api/",
        params={
            "type": "all-meat",
            "paras": paragraphs,
            "start-with-lorem": 1,
            "format": "text",
        }
    )
    response.raise_for_status()
    return response.text
