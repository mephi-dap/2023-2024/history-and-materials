class Bar:
    def __init__(self):
        self.value = 0

    def increment(self):
        self.value += 1

    def print_value(self):
        print(self.value)

    @property
    def super_value(self):
        return self.value