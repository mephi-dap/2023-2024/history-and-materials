import hello_package


def main():
    hello_package.print_hi("test")
    print(hello_package.get_lorem_ipsum(2))


if __name__ == '__main__':
    main()