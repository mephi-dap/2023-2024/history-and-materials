#include <vector>
#include <iostream>
#include <random>
#include <thread>

void PrepareNumbers(std::vector<int>& numbers, size_t amount) {
    std::random_device rd;
    std::mt19937 random(rd());
    numbers.reserve(amount);

    for (size_t i = 0; i < amount; ++i) {
        numbers.push_back(random());
    }
}

int Sum(const std::vector<int>& numbers) {
    std::vector<std::thread> threads;
    int resultSum = 0;
    constexpr size_t threadsAmount = 5;

    auto threadFunc = [&numbers, &resultSum] (size_t threadIdx) {
        const size_t fromIdx = numbers.size() / threadsAmount * threadIdx;
        const size_t toIdx = fromIdx + numbers.size() / threadsAmount;

        for (size_t i = fromIdx; i < toIdx; ++i) {
            // Тут происходит чтение старого значения и запись увеличенного. Что может пойти не так?
            resultSum += numbers[i];

            /*
             * Чтение и запись из разных потоков не упорядочены, тут data race.
             * В результате можем получить неправильные результаты, поэтому тут также и race condition.
             * Объяснение на примере:
             * - Пусть в resultSum сейчас лежит значение 150
             * - Поток №1 прочитал из resultSum значение 150
             * - Поток №2 тоже прочитал из resultSum значение 150
             * - Поток №1 прибавил свое число и получил значение 150 + 10 = 160, затем записал его в resultSum
             * - Поток №2 прибавил свое число и получил значение 150 + 20 = 170, затем записал его в resultSum
             * - В итоге в resultSum лежит значение 170, хотя по смыслу оба потока должны были прибавить свои числа
             * - Вместо 180 получили в результате значение 170
             */
        }
    };

    for (size_t i = 0; i < threadsAmount; ++i) {
        threads.emplace_back(threadFunc, i);
    }

    for (auto& thread : threads) {
        thread.join();
    }

    return resultSum;
}

int main() {
    constexpr size_t amount = 100000000;
    std::vector<int> numbers;

    PrepareNumbers(numbers, amount);

    std::cout << Sum(numbers) << std::endl;
    std::cout << Sum(numbers) << std::endl;
    std::cout << Sum(numbers) << std::endl;
    std::cout << Sum(numbers) << std::endl;
    std::cout << Sum(numbers) << std::endl;

    return 0;
}