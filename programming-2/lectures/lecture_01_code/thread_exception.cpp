#include <vector>
#include <iostream>
#include <thread>

void BadFunc() {
    throw 1;
}

int main() {
    std::vector<std::thread> threads;
    for (size_t i = 0; i < 10; ++i) {
        threads.emplace_back(BadFunc);
    }

    for (auto& thread : threads) {
        thread.join();
    }

    return 0;
}
