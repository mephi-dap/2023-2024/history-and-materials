#include <vector>
#include <iostream>
#include <thread>

void SayHello() {
    std::cout << "Hello" << ", " << "World" << "!" << std::endl;
    // Строка, которую мы передаем в каждом вызове оператора <<, выводится полностью
    // и не перемешивается с другими выводимыми строками.
    // При этом между отдельными вызовами оператора << может "вклиниться" другой поток и вывести что-то свое в поток.
    // Если мы хотели выводить фразу "Hello, World!" целиком, то надо было передать её в один вызов оператора <<,
    // иначе мы получили race condition
}

int main() {
    std::vector<std::thread> threads;

    for (size_t i = 0; i < 10; ++i) {
        threads.emplace_back(SayHello);
    }

    for (auto& thread : threads) {
        thread.join();
    }

    return 0;
}
