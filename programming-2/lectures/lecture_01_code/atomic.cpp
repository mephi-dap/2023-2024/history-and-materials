#include <vector>
#include <iostream>
#include <random>
#include <thread>
#include <shared_mutex>

std::mutex stdoutMutex;

int Sum(const std::vector<int>& numbers) {
    std::vector<std::thread> threads;
    std::atomic<int> resultSum = 0;  // мьютексы не нужны для работы с atomic
    int nonAtomicSum = 0;  // для примера сравним с обычным int
    constexpr size_t threadsAmount = 4;

    auto threadFunc = [&numbers, &resultSum, &nonAtomicSum] (size_t threadIdx) {
        const size_t fromIdx = numbers.size() / threadsAmount * threadIdx;
        const size_t toIdx = fromIdx + numbers.size() / threadsAmount;

        for (size_t i = fromIdx; i < toIdx; ++i) {
            // Чтение и запись происходят атомарно на уровне процессора.
            // Не может быть такого, что мы прочитаем значение, потом другой поток его перезапишет, а потом мы запишем
            // свой результат сложения.
            resultSum += numbers[i];  // аналогично resultSum.fetch_add(numbers[i])

            // А тут будет обычное чтение и запись без atomic. Между чтением и записью может прийти другой поток
            // и что-то сделать с данными в переменной nonAtomicSum
            nonAtomicSum += numbers[i];
        }
    };

    for (size_t i = 0; i < threadsAmount; ++i) {
        threads.emplace_back(threadFunc, i);
    }

    for (auto& thread : threads) {
        thread.join();
    }

    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Atomic sum: " << resultSum << std::endl;
    std::cout << "Non-atomic sum: " << nonAtomicSum << std::endl;

    return resultSum;
}

class AverageCalculator {
public:
    int GetAverage() {
        std::shared_lock<std::shared_mutex> sharedLock(sharedMutex_);
        // Много потоков могут зайти сюда одновременно, так как shared_lock позволяет это
        return Sum(numbers_) / numbers_.size();
    }

    void AppendValue(int value) {
        std::unique_lock<std::shared_mutex> uniqueLock(sharedMutex_);
        // Только один поток может взять unique lock в один момент времени.
        // Более того, когда какой-то поток зашел сюда, другие потоки также не могут взять shared_lock в
        // методе GetAverage()
        numbers_.push_back(value);
    }
private:
    std::vector<int> numbers_;
    std::shared_mutex sharedMutex_;
};

void PrepareNumbers(AverageCalculator& av, size_t amount) {
    std::random_device rd;
    std::mt19937 random(rd());

    for (size_t i = 0; i < amount; ++i) {
        av.AppendValue(random());
    }
}

void PrintAverage(AverageCalculator& av) {
    int result = av.GetAverage();

    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Average of vector: " << result << std::endl << std::endl;
}

int main() {
    constexpr size_t amount = 1000000;
    AverageCalculator av;

    std::vector<std::thread> writeThreads, readThreads;
    for (size_t i = 0; i < 10; ++i) {
        writeThreads.emplace_back(PrepareNumbers, std::ref(av), amount);
        readThreads.emplace_back(PrintAverage, std::ref(av));
    }

    for (auto& thread : writeThreads) {
        thread.join();
    }

    for (auto& thread : readThreads) {
        thread.join();
    }

    return 0;
}
