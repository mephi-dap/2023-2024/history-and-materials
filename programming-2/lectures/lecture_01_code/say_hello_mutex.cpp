#include <vector>
#include <iostream>
#include <thread>
#include <mutex>

using namespace std::chrono_literals;

std::mutex stdoutMutex;

void SayHelloManualMutex() {
    stdoutMutex.lock();
    std::cout << "Hello" << ", " << "World" << "!" << std::endl;
    stdoutMutex.unlock();  // если выполнение функции не дойдет до этой строчки, то мьютекс останется заблокированным
}

void SayHelloLockGuard() {
    std::lock_guard<std::mutex> lock(stdoutMutex);  // принцип RAII в применении к блокировке мьютекса
    std::cout << "Hello" << ", " << "World" << "!" << std::endl;
}

void SayHelloUniqueLock() {
    std::unique_lock<std::mutex> lock(stdoutMutex);  // принцип RAII в применении к блокировке мьютекса
    std::cout << "Hello" << ", " << "World" << "!" << std::endl;
    lock.unlock();  // unique_lock можно разблокировать вручную, если хочется
    std::this_thread::sleep_for(1s);  // 1s из std::chrono_literals
    lock.lock(); // и опять заблокировать
    std::cout << "Waited" << std::endl;
}

template<typename F>
void RunInThreads(F func) {
    std::vector<std::thread> threads;
    for (size_t i = 0; i < 10; ++i) {
        threads.emplace_back(func);
    }

    for (auto& thread : threads) {
        thread.join();
    }
}

int main() {
    std::cout << "SayHelloManualMutex():\n";
    RunInThreads(SayHelloManualMutex);

    std::cout << "SayHelloLockGuard():\n";
    RunInThreads(SayHelloLockGuard);

    std::cout << "SayHelloUniqueLock():\n";
    RunInThreads(SayHelloUniqueLock);

    return 0;
}
