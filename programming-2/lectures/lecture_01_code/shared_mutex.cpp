#include <vector>
#include <iostream>
#include <random>
#include <thread>
#include <shared_mutex>

int Sum(const std::vector<int>& numbers) {
    std::vector<std::thread> threads;
    int resultSum = 0;
    constexpr size_t threadsAmount = 5;
    std::mutex sumMutex;

    auto threadFunc = [&numbers, &resultSum, &sumMutex] (size_t threadIdx) {
        const size_t fromIdx = numbers.size() / threadsAmount * threadIdx;
        const size_t toIdx = fromIdx + numbers.size() / threadsAmount;
        int localSum = 0;

        for (size_t i = fromIdx; i < toIdx; ++i) {
            localSum += numbers[i];
        }

        std::lock_guard<std::mutex> lock(sumMutex);
        resultSum += localSum;
    };

    for (size_t i = 0; i < threadsAmount; ++i) {
        threads.emplace_back(threadFunc, i);
    }

    for (auto& thread : threads) {
        thread.join();
    }

    return resultSum;
}

class AverageCalculator {
public:
    int GetAverage() {
        std::shared_lock<std::shared_mutex> sharedLock(sharedMutex_);
        // Много потоков могут зайти сюда одновременно, так как shared_lock позволяет это
        return Sum(numbers_) / numbers_.size();
    }

    void AppendValue(int value) {
        std::unique_lock<std::shared_mutex> uniqueLock(sharedMutex_);
        // Только один поток может взять unique lock в один момент времени.
        // Более того, когда какой-то поток зашел сюда, другие потоки также не могут взять shared_lock в
        // методе GetAverage()
        numbers_.push_back(value);
    }
private:
    std::vector<int> numbers_;
    std::shared_mutex sharedMutex_;
};

void PrepareNumbers(AverageCalculator& av, size_t amount) {
    std::random_device rd;
    std::mt19937 random(rd());

    for (size_t i = 0; i < amount; ++i) {
        av.AppendValue(random());
    }
}

std::mutex stdoutMutex;

void PrintAverage(AverageCalculator& av) {
    int result = av.GetAverage();

    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << result << std::endl;
}

int main() {
    constexpr size_t amount = 1000000;
    AverageCalculator av;

    std::vector<std::thread> writeThreads, readThreads;
    for (size_t i = 0; i < 10; ++i) {
        writeThreads.emplace_back(PrepareNumbers, std::ref(av), amount);
        readThreads.emplace_back(PrintAverage, std::ref(av));
    }

    for (auto& thread : writeThreads) {
        thread.join();
    }

    for (auto& thread : readThreads) {
        thread.join();
    }

    return 0;
}
