#include <algorithm>
#include <random>
#include <vector>

using namespace std;

using ll = long long;

// генератор случайных чисел
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

// КОД АЛГОРИТМА
ll merge_sort_with_inversions(int l, int r, vector<int>& vec) {
    // краевые случаи
    // если поделим отрезок длины 3, получим отрезки длины 1 и 2
    if (r - l == 1)
        return 0;

    if (r - l == 2) {
        ll res = vec[l] > vec[l + 1];
        if (vec[l] > vec[l + 1])
            swap(vec[l], vec[l + 1]);
        return res;
    }

    int mid = (l + r) >> 1;
    ll ans = merge_sort_with_inversions(l, mid, vec);
    ans += merge_sort_with_inversions(mid, r, vec);

    // можно снаружи задать бафер
    vector<int> buffer;
    buffer.reserve(r - l);
    for (int i = l, j = mid; i < mid || j < r;) {
        if (i == mid) {
            buffer.emplace_back(vec[j]);
            ++j;
        } else if (j == r) {
            buffer.emplace_back(vec[i]);
            ++i;
        } else {
            if (vec[i] <= vec[j]) {
                buffer.emplace_back(vec[i]);
                ++i;
            } else {
                // все не рассмотренные элементы из левой части больше
                ans += mid - i;
                buffer.emplace_back(vec[j]);
                ++j;
            }
        }
    }

    for (int i = l; i < r; ++i) {
        vec[i] = buffer[i - l];
    }

    return ans;
}
// КОНЕЦ КОДА АЛГОРИТМА

// Брутфорс
ll brute(const vector<int>& vec) {
    ll ans = 0;
    for (int i = 1; i < vec.size(); ++i) {
        for (int j = 0; j < i; ++j) {
            ans += vec[i] < vec[j];
        }
    }
    return ans;
}

// Тесты
void check(vector<int> vec) {
    ll etalon = brute(vec);
    ll actual = merge_sort_with_inversions(0, vec.size(), vec);
    assert(is_sorted(vec.begin(), vec.end()));
    assert(etalon == actual);
}

// проверка всех перестановок длины n
void test(int n) {
    vector<int> p(n);
    iota(p.begin(), p.end(), 1);
    do {
        check(p);
    } while(next_permutation(p.begin(), p.end()));
}

int main() {
    // все перестановки небольшой длины
    for (int i = 1; i < 10; ++i) {
        test(i);
    }

    // несколько "случайных" перестановок большой длины
    for (int i = 0; i < 10; ++i) {
         int n = 1e4;
        vector<int> p(n);
        iota(p.begin(), p.end(), 1);
        shuffle(p.begin(), p.end(), rng);
        check(p);
    }

    return 0;
}
