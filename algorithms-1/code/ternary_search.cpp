#include <iostream>
#include <vector>
#include <deque>
#include <cmath>

double f(double x, double Vp, double Vf, double a) {
    return std::sqrt((1-a)*(1-a) + x*x) / Vp + std::sqrt(a*a +(1-x)*(1-x)) / Vf;  
}

double res(double Vp, double Vf, double a) {
    double l = 0;
    double r = 1;
    double eps = 0.00001;
    while (r - l > eps)
    {
        double m1 = l + (r-l)/3;
        double m2 = r - (r-l)/3;

        if (f(m1,Vp,Vf,a) < f(m2,Vp,Vf,a)) {
            r = m2;
        } else {
            l = m1;
        }
    }
    
    return l;
}

int main() {
    std::cout << res(5,3,0.4);
    return 0;
}
